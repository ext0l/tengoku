# [tengoku](https://www.youtube.com/watch?v=psX1hLeUDFA) - bullet heaven

Experimenting with bevy and rust.

![A screenshot of tengoku](screenshot.png)

## Setup

Run `just setup-repo` to... set up the repo. This is a once-off script.

If you have direnv and flake support, the `direnv` integration will take care of it. `nix develop` will also work. If you're not using nix, `flake.nix` lists all the tools you'll need.

## What's in a name?

"Bullet heaven" is a name I've seen for games along the lines of [Vampire Survivors](https://store.steampowered.com/app/1794680/Vampire_Survivors/) (which is great, you should 1000% play it) where the player character has a set of attack that they constantly use with no player input that fill the screen with projectiles. It's a play off the idea of [bullet hell](https://en.wikipedia.org/wiki/Shoot_%27em_up); instead of *dodging* the projectiles, you generate them. Tengoku is the Japanese word for "heaven" (modulo cultural differences). ".59", the song linked at the top of the README, has a name that would be pronounced "ten go kyu", which is very close to "tengoku". I also just like it.
