struct BulletTrail {
    color: vec4<f32>,
};

@group(1) @binding(0)
var<uniform> material: BulletTrail;

@fragment
fn fragment(
  #import bevy_sprite::mesh2d_vertex_output
) -> @location(0) vec4<f32> {
  return vec4<f32>(material.color.xyz, material.color.w * uv.x);
}
