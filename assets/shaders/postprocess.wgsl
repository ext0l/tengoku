struct DistortionStorage {
 center: vec2<f32>,
 radius: f32,
 strength: f32
};

struct DistortionConfig {
  seed: vec2<f32>,
  time: f32,
}


@group(2) @binding(0)
var<uniform> config: DistortionConfig;
@group(2) @binding(1)
var<uniform> distortions: array<DistortionStorage, 8u>; // keep length in sync
@group(2) @binding(2)
var base_texture: texture_2d<f32>;
@group(2) @binding(3)
var base_sampler: sampler;

// Apply barrel distortion to a vector using Brown-Conrady.
fn distort(v: vec2<f32>, radius: f32, strength: f32) -> vec2<f32> {
  let normalized = v / radius;
  let r2 = dot(normalized, normalized);
  // values chosen for aesthetics.
  return radius * normalized * (1.0 - 1.0 * r2 * strength);
}

// Generate a random vec2 with coordinates in the range [-1, 1).
fn random(uv: vec2<f32>) -> vec2<f32> {
  // all numbers here are effectively arbitrary.
  let wx = vec2(283.1 * uv.x + 110.3 * uv.y, 111.0 * uv.x - 94.0 * uv.y);
  return (fract(sin(wx) * 43758.2394) - 0.5) * 2.0;
}

// Returns a value that rapidly oscillates in the range [-1, 1] in a 'jittery' way.
fn jitter(pos: vec2<f32>, time: f32) -> f32{
  return sin(sin(time * 8.0 + dot(pos, vec2(50.8, -39.1))) * 8.0);
}

const MAX_FUZZ: f32 = 0.005;
const MAX_LENS: f32 = 1.0;
const MAX_BRIGHTEN: f32 = 16.0;
  
#import bevy_sprite::mesh2d_vertex_output::VertexOutput

@fragment
fn fragment(
  in: VertexOutput
) -> @location(0) vec4<f32> {
  // uv that we *actually* sample from
  var fuzzed_uv = in.uv;
  // we multiply the final color by this
  var brighten = 1.0;
  for (var i = 0u; i < 8u; i++) {
    if (distortions[i].radius == 0.0) {
        continue;
    }
    // all of this is designed to visually indicate that this indicates
    // Something Wrong is going on here.
    let center = distortions[i].center;
    let radius = distortions[i].radius * (1. + 0.05 * jitter(center, config.time));

    // uv offset relative to the center of the distortion
    let offset = fuzzed_uv - center;
    if (length(offset) < radius) {
      // the relative powers here are to make the fuzz fade first and the
      // brightness fade last.
      let strength = distortions[i].strength;
      let lens_strength = MAX_LENS * strength;
      let fuzz_strength = MAX_FUZZ * strength * strength;
      let brighten_strength = MAX_BRIGHTEN * sqrt(strength);

      fuzzed_uv = center + distort(offset, radius, lens_strength);
      fuzzed_uv += fuzz_strength * random(offset + config.seed);
      let distance = length(offset) / radius;
      // brighten areas near edges of the distortion.
      brighten += brighten_strength * pow(distance, 6.0);
    }
  }

  return textureSample(base_texture, base_sampler, fuzzed_uv) * brighten;
}
