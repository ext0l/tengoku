<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.0" name="obstacles-and-objects" tilewidth="16" tileheight="16" tilecount="256" columns="16">
 <image source="obstacles-and-objects.png" width="256" height="256"/>
 <tile id="32">
  <objectgroup draworder="index" id="2">
   <object id="1" x="13" y="8" width="3" height="4"/>
  </objectgroup>
 </tile>
 <tile id="33">
  <objectgroup draworder="index" id="4">
   <object id="4" x="0" y="8" width="3" height="4"/>
  </objectgroup>
 </tile>
</tileset>
