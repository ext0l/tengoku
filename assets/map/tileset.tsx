<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.0" name="tileset" tilewidth="16" tileheight="16" tilecount="256" columns="16">
 <transformations hflip="0" vflip="0" rotate="0" preferuntransformed="1"/>
 <image source="tileset.png" width="256" height="256"/>
 <tile id="2" type="wall"/>
 <tile id="3" type="wall"/>
 <tile id="4" type="wall"/>
 <tile id="5" type="wall"/>
 <tile id="6" type="wall"/>
 <tile id="7" type="wall"/>
 <tile id="18" type="wall"/>
 <tile id="19" type="wall"/>
 <tile id="20" type="wall"/>
 <tile id="21" type="wall"/>
 <tile id="22" type="wall"/>
 <tile id="23" type="wall"/>
 <tile id="34" type="wall"/>
 <tile id="35" type="wall"/>
 <tile id="36" type="wall"/>
 <tile id="37" type="wall"/>
 <tile id="38" type="wall"/>
 <wangsets>
  <wangset name="WallSet" type="edge" tile="35">
   <wangcolor name="Wall" color="#ff0000" tile="-1" probability="1"/>
   <wangcolor name="Pavement" color="#00ff00" tile="-1" probability="1"/>
   <wangtile tileid="0" wangid="2,0,2,0,2,0,2,0"/>
   <wangtile tileid="3" wangid="2,0,2,0,1,0,2,0"/>
   <wangtile tileid="4" wangid="2,0,1,0,1,0,2,0"/>
   <wangtile tileid="5" wangid="2,0,2,0,1,0,1,0"/>
   <wangtile tileid="6" wangid="1,0,1,0,1,0,2,0"/>
   <wangtile tileid="7" wangid="1,0,2,0,1,0,1,0"/>
   <wangtile tileid="18" wangid="1,0,2,0,2,0,2,0"/>
   <wangtile tileid="19" wangid="1,0,2,0,1,0,2,0"/>
   <wangtile tileid="20" wangid="1,0,1,0,2,0,2,0"/>
   <wangtile tileid="21" wangid="1,0,2,0,2,0,1,0"/>
   <wangtile tileid="22" wangid="2,0,1,0,1,0,1,0"/>
   <wangtile tileid="23" wangid="1,0,1,0,2,0,1,0"/>
   <wangtile tileid="34" wangid="2,0,1,0,2,0,2,0"/>
   <wangtile tileid="35" wangid="1,0,1,0,1,0,1,0"/>
   <wangtile tileid="36" wangid="2,0,1,0,2,0,1,0"/>
   <wangtile tileid="37" wangid="2,0,2,0,2,0,1,0"/>
  </wangset>
 </wangsets>
</tileset>
