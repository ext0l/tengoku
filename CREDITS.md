# Credits for free assets

This file contains the credits for all assets maintained in this repo.

## [Robot Warfare Asset Pack](https://mattwalkden.itch.io/free-robot-warfare-pack)

From [Matt Walkden](https://mattwalkden.itch.io/), licensed CC0.

- map/obstacles-and-objects.png
- map/tileset.png
- sprites/assault.png
- sprites/bullet.png
- sprites/player.png
- sprites/rpg-round.png
- sprites/small-explosion.png

## [Silver](https://poppyworks.itch.io/silver)

From [Poppy Works](https://poppyworks.itch.io/), licensed CC-BY-4.0 for all games with total spend/revenue less than $100,000.

- fonts/Silver.ttf

## [Q009's Weapon Sounds](https://opengameart.org/content/q009s-weapon-sounds)

Licensed [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).

- shotgun.ogg
- rocket-launcher.ogg

## [Kenney's Sci-Fi Sounds](https://kenney.nl/assets/sci-fi-sounds)

- laser.ogg
- shot.ogg

## [Severin Meyer](https://sev.dev/fonts/)

- assets/Oxanium-*.ttf, licensed under the SIL OFL found at assets/LICENSE.Oxanium.txt

## [gnsh](https://opengameart.org/users/gnsh)

- assets/ui/damage_font.png

## My own work :)

My own creations. Dual-licensed under [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/) (for consistency with the art world) and MIT (for consistency with the rest of the code); pick whichever one works for you.

- All `.animation.toml` files
- All `.tmx` and `.tsx` files
- All shaders
- sprites/laser.png
- sprites/laser-flash.png
