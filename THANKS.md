Thanks to my partners for the ideas, inspirations, and gentle ass-kicking for focus.

Thanks to everyone in the official Bevy discord who answered questions, and to the developers of Bevy for making an engine that feels *fun* to use.

Of course, thanks to the developers of every crate I used as well as Rust itself.

And it's cliche to say, but thank *you* for playing this game or even just looking at it.


