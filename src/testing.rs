//! Code used for testing the individual parts of the application.
use std::time::Duration;

use bevy::{
    app::{PluginGroupBuilder, PluginsState},
    log::LogPlugin,
    prelude::*,
    time::TimePlugin,
};
use bevy_asset_loader::prelude::*;

use crate::scheduling::GameState;

/// A minimal set of plugins useful for testing. Feel free to add more things to
/// this, but avoid anything that will cause significant logspam.
pub struct CommonTestPlugins;

impl PluginGroup for CommonTestPlugins {
    fn build(self) -> PluginGroupBuilder {
        MinimalPlugins
            .build()
            .add(LogPlugin::default())
            .add(AssetPlugin::default())
            .add(ImagePlugin::default())
            .add(TimePlugin)
            .add_after::<AssetPlugin, _>(TestPlugin::default())
    }

    fn name() -> String {
        "TestPlugins".to_owned()
    }
}

/// Defines test plugin c
pub struct TestPlugin {
    /// If true, transitioning to [`GameState::LoadingError`] causes a panic.
    /// Defaults to true.
    panic_on_load_failure: bool,
    /// Causes a panic after this duration; effectively acts as a test timeout.
    /// You should still set a timeout in your test runner in case this gets
    /// stuck in a single loop. Defaults to 2 seconds.
    timeout: Option<Duration>,
}

#[derive(Resource)]
struct Timeout(Duration);

fn load_failure_panic() {
    panic!("Some assets failed to load!");
}

fn check_timeout(timeout: Res<Timeout>, time: Res<Time>) {
    dbg!(time.elapsed(), timeout.0);
    if time.elapsed() > timeout.0 {
        panic!(
            "Elapsed time {:?} greater than timeout {:?}",
            time.elapsed(),
            timeout.0
        )
    }
}

impl Default for TestPlugin {
    fn default() -> Self {
        Self {
            panic_on_load_failure: true,
            timeout: Some(Duration::from_secs(2)),
        }
    }
}

impl Plugin for TestPlugin {
    fn build(&self, app: &mut App) {
        app.init_state::<GameState>().add_loading_state(
            LoadingState::new(GameState::Loading)
                .continue_to_state(GameState::Running)
                .on_failure_continue_to_state(GameState::LoadingError),
        );
        if self.panic_on_load_failure {
            app.add_systems(OnEnter(GameState::LoadingError), load_failure_panic);
        }
        if let Some(timeout) = self.timeout {
            app.insert_resource(Timeout(timeout));
            app.add_systems(Update, check_timeout);
        }
        // Tests often use large time deltas.
        app.add_systems(PreStartup, |mut res: ResMut<Time<Virtual>>| {
            res.set_max_delta(Duration::MAX)
        });
    }
}

pub fn initialize_plugins(app: &mut App) {
    while app.plugins_state() != PluginsState::Ready {
        bevy::tasks::tick_global_task_pools_on_main_thread();
    }
    app.finish();
    app.cleanup();
}

#[cfg(test)]
mod tests {
    use bevy::{ecs::schedule::ExecutorKind, time::TimeUpdateStrategy};
    use bevy_asset_loader::prelude::*;

    use super::*;

    #[test]
    #[should_panic(expected = "Some assets failed to load")]
    fn load_failure_causes_panic() {
        #[derive(AssetCollection, Resource)]
        struct BadAssets {
            #[asset(path = "i do not exist.png")]
            #[allow(dead_code)]
            secret_plans: Handle<Image>,
        }
        let mut app = App::new();
        app.add_plugins(CommonTestPlugins).add_loading_state(
            LoadingState::new(GameState::Loading).load_collection::<BadAssets>(),
        );

        // mark these as single-threaded so that the panic actually comes through;
        // otherwise, we get "unwrap on None" panics that aren't useful. we do in fact
        // need *both* of these.
        app.edit_schedule(OnEnter(GameState::LoadingError), |s| {
            s.set_executor_kind(ExecutorKind::SingleThreaded);
        });
        app.edit_schedule(Main, |s| {
            s.set_executor_kind(ExecutorKind::SingleThreaded);
        });

        for _ in 0..1000 {
            app.update();
        }
        panic!("we should have panicked in app.update(), but didn't?");
    }

    #[test]
    #[should_panic(expected = "greater than timeout")]
    fn exceeding_timeout_causes_panic() {
        let mut app = App::new();
        app.add_plugins(CommonTestPlugins);
        app.update();
        app.insert_resource(TimeUpdateStrategy::ManualDuration(Duration::from_secs(5)));
        app.edit_schedule(Main, |s| {
            s.set_executor_kind(ExecutorKind::SingleThreaded);
        });
        app.update();
    }
}
