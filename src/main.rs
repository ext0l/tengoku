mod actor;
mod damage;
mod debug;
mod despawn_timer;
mod dps_tracker;
mod enemy;
mod physics;
mod player;
mod prelude;
mod prune;
mod render;
mod scheduling;
#[cfg(test)]
mod testing;
mod tiled_tilemap;
mod ui;
mod weapons;

use bevy::window::WindowResolution;
use bevy_kira_audio::prelude::*;
use bevy_tweening::TweeningPlugin;
use damage::DamagePlugin;
use debug::debug_mode_plugin;
use dps_tracker::dps_tracker_plugin;
use enemy::EnemyPlugin;
use extol_pixel_font::PixelFontPlugin;
use extol_sprite_layer::SpriteLayerPlugin;
use physics::common_physics_plugin;
use player::player_plugin;
use prelude::*;
use prune::prune_plugin;
use render::{animation::SlideshowAnimationPlugin, camera_plugin, ZLayer};
use scheduling::{common_scheduling_plugin, GameState};
use tiled_tilemap::TiledMapPlugin;
use ui::ui_plugin;
use weapons::WeaponsPlugin;

fn ensure_plugin<P: Plugin>(app: &mut App, plugin: P) {
    if !app.is_plugin_added::<P>() {
        app.add_plugins(plugin);
    }
}

fn main() {
    let mut app = App::new();

    app.init_state::<GameState>()
        .fn_plugin(common_scheduling_plugin)
        .add_loading_state(
            LoadingState::new(GameState::Loading).continue_to_state(GameState::MainMenu),
        )
        .add_plugins(
            DefaultPlugins
                .set(WindowPlugin {
                    // TODO: borderless fullscreen or something? eh.
                    primary_window: Some(Window {
                        resolution: WindowResolution::new(1280., 960.),
                        decorations: true,
                        resizable: false,
                        title: "tengoku".to_string(),
                        ..default()
                    }),
                    ..default()
                })
                .set(ImagePlugin::default_nearest()),
        )
        .fn_plugin(debug_mode_plugin)
        .insert_resource(ClearColor(Color::rgb(0.05, 0.05, 0.05)))
        .insert_resource(Msaa::Off)
        .add_plugins((
            WeaponsPlugin,
            PixelFontPlugin,
            DamagePlugin,
            EnemyPlugin,
            TiledMapPlugin,
            AudioPlugin,
            SlideshowAnimationPlugin,
            TweeningPlugin,
            SpriteLayerPlugin::<ZLayer>::default(),
        ))
        .fn_plugin(common_physics_plugin)
        .fn_plugin(player_plugin)
        .fn_plugin(ui_plugin)
        .fn_plugin(camera_plugin)
        .fn_plugin(dps_tracker_plugin)
        .fn_plugin(prune_plugin);

    app.add_plugins(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(10.0))
        .insert_resource(RapierConfiguration {
            gravity: Vec2::ZERO,
            ..default()
        })
        .run();
}
