use std::time::Duration;

use seldom_state::prelude::*;

use super::{
    ai::{
        state,
        trigger::{self},
        WaitTimer,
    },
    EnemyAssets,
};
use crate::{
    prelude::*,
    render::{
        animation::{AnimationMode, AnimationState, SetAnimation},
        ZLayer,
    },
};

/// Used to delay between when the sniper locks on and when they actually fire.
#[derive(Clone, Copy, Reflect, Component)]
pub struct Prefire;

#[derive(Clone, Copy, Reflect, Component)]
pub(super) struct Aiming {
    target: Entity,
}

pub(super) fn sniper_ai(target: Entity, speed: f32) -> StateMachine {
    let near = trigger::near(target, 85.0);
    let chasing = state::Chasing { target, speed };
    StateMachine::default()
        .trans::<AnyState, _>(trigger::zero_hp, state::Dead)
        .trans::<state::Idle, _>(seldom_state::trigger::always, chasing)
        .trans::<state::Chasing, _>(near, Aiming { target })
        .trans::<Aiming, _>(trigger::has_child_with::<state::LockedOn>, Prefire)
        .on_enter::<Prefire>(|e| {
            e.insert(WaitTimer::new(Duration::from_millis(500)));
        })
        .trans::<Prefire, _>(trigger::wait_finished, state::Firing)
        .on_enter::<state::Firing>(|e| {
            e.insert(SetAnimation::single("fire"));
            e.despawn_descendants();
        })
        .trans::<state::Firing, _>(trigger::animation_finished, state::Idle)
        .on_enter::<Aiming>(|e| {
            e.insert(SetAnimation::repeat("idle"));
        })
        .on_enter::<state::Chasing>(|e| {
            e.insert(SetAnimation::repeat("walk"));
        })
        .on_enter::<state::Dead>(|e| {
            e.insert(SetAnimation::single("death"));
        })
}

fn reticle_ai(target: Entity) -> StateMachine {
    StateMachine::default()
        .trans::<state::Chasing, _>(trigger::near(target, 1.0), state::LockedOn { target })
        .on_enter::<state::LockedOn>(|e| {
            e.insert(SetAnimation::repeat("locked_on"));
        })
}

/// When an entity enters the aiming state, spawn a reticle.
pub(super) fn spawn_reticles(
    mut commands: Commands,
    aiming: Query<(Entity, &Aiming), Added<Aiming>>,
    assets: Res<EnemyAssets>,
) {
    for (entity, Aiming { target }) in &aiming {
        commands.entity(entity).with_children(|entity| {
            entity.spawn((
                Name::from("reticle"),
                state::Chasing {
                    target: *target,
                    speed: 60.0,
                },
                reticle_ai(*target),
                SpriteSheetBundle {
                    atlas: TextureAtlas {
                        layout: assets.sniper_target_layout.clone(),
                        index: 0,
                    },
                    texture: assets.sniper_target.clone(),
                    ..default()
                },
                RigidBody::KinematicVelocityBased,
                LockedAxes::ROTATION_LOCKED,
                Velocity::zero(),
                ZLayer::AbovePlayer,
                AnimationState::new(
                    "idle",
                    AnimationMode::RepeatForever,
                    assets.sniper_target_animation.clone(),
                ),
            ));
        });
    }
}
