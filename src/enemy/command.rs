//! Defines [`SpawnCommand`], a [`Command`] for spawning enemies at a given
//! position.
use std::time::Duration;

use bevy::ecs::system::Command;
use bevy_kira_audio::AudioSource;
use seldom_state::prelude::*;

use super::{
    ai::{state, trigger, WaitTimer},
    sniper::sniper_ai,
    EnemyAssets,
};
use crate::{
    actor::{ActorParams, Enemy, Player},
    damage::{DamageMethod, DamageOnHit, HP},
    enemy::{EnemyKind, FireAudio},
    physics::{GROUP_ENEMY_HITBOX, GROUP_ENEMY_HURTBOX, GROUP_PLAYER_HURTBOX},
    player::xp::DropsXp,
    prelude::*,
    prune::PruneIfOffscreen,
    render::{
        animation::{AnimationMode, AnimationState, Animations, SetAnimation},
        AutoFlip, ZLayer,
    },
    scheduling::RunningTime,
};

#[derive(Clone, Debug)]
pub struct SpawnEnemy {
    pub kind: EnemyKind,
    pub position: Vec2,
}

impl Command for SpawnEnemy {
    fn apply(self, world: &mut World) {
        let assets = world
            .get_resource::<EnemyAssets>()
            .expect("spawning enemies requires EnemyAssets");
        let blueprint = EnemyBlueprint::from(self.kind, assets);

        let mut query = world.query_filtered::<Entity, With<Player>>();
        let Ok(player) = query.get_single(world) else {
            warn!("Tried to spawn an enemy with no player");
            return;
        };

        let ai = match self.kind {
            EnemyKind::Assault | EnemyKind::Leader => assault_ai(player, blueprint.speed),
            EnemyKind::Sniper => sniper_ai(player, blueprint.speed),
        };

        let running_time = world
            .get_resource::<RunningTime>()
            .expect("spawning enemies requires RunningTime");
        let hp_scaling = 1.0 + (running_time.as_secs_f32() / 120.0).powf(2.0);

        let mut entity_commands = world.spawn((
            Name::new(format!("{:?}", self.kind)),
            Enemy,
            PruneIfOffscreen::new(Duration::from_secs(1)),
            HP((blueprint.hp as f32 * hp_scaling).round() as i32),
            ActorParams {
                // Note that *this* collider represents the enemy's hurtbox.
                collider: blueprint.collider.clone(),
            }
            .into_bundle(),
            ZLayer::Enemy,
            SpriteSheetBundle {
                atlas: TextureAtlas {
                    layout: blueprint.layout,
                    index: 0,
                },
                texture: blueprint.texture,
                transform: Transform::from_translation(self.position.extend(0.0)),
                ..default()
            },
            ai,
            state::Idle,
            AnimationState::new(
                "walk",
                AnimationMode::RepeatForever,
                blueprint.animation.clone(),
            ),
            CollisionGroups::new(GROUP_ENEMY_HURTBOX, Group::ALL),
            AutoFlip,
            Friction::new(0.0),
            DropsXp {
                amount: 1,
                chance: 0.5,
            },
            FireAudio(blueprint.fire_audio),
        ));
        entity_commands.with_children(|parent| {
            // This is the enemy's hitbox, which is what actually damages the player when
            // they hit it.
            parent.spawn((
                Name::new("Hitbox"),
                blueprint.collider.clone(),
                CollidingEntities::default(),
                ActiveEvents::COLLISION_EVENTS,
                DamageOnHit {
                    amount: blueprint.damage as i32,
                    method: DamageMethod::Enemy,
                },
                CollisionGroups::new(GROUP_ENEMY_HITBOX, GROUP_PLAYER_HURTBOX),
                Sensor,
                SpatialBundle::default(),
            ));
        });
    }
}

/// Describes enough information about an enemy to build it.
struct EnemyBlueprint {
    hp: u32,
    speed: f32,
    damage: u32,
    collider: Collider,
    layout: Handle<TextureAtlasLayout>,
    texture: Handle<Image>,
    animation: Handle<Animations>,
    fire_audio: Handle<AudioSource>,
}

impl EnemyBlueprint {
    pub fn from(kind: EnemyKind, assets: &EnemyAssets) -> Self {
        use EnemyKind::*;
        let collider = Collider::ball(5.);
        let layout = assets.layout(kind);
        let texture = assets.texture(kind);
        let animation = assets.animation.clone();
        match kind {
            Assault => Self {
                hp: 20,
                speed: 35.0,
                damage: 1,
                collider,
                layout,
                texture,
                animation,
                fire_audio: assets.shot_sfx.clone(),
            },
            Leader => Self {
                hp: 35,
                speed: 25.0,
                damage: 2,
                collider,
                layout,
                texture,
                animation,
                fire_audio: assets.shot_sfx.clone(),
            },
            Sniper => Self {
                hp: 20,
                speed: 65.0,
                damage: 10,
                collider,
                layout,
                texture,
                animation,
                fire_audio: assets.snipe_sfx.clone(),
            },
        }
    }
}

fn assault_ai(target: Entity, speed: f32) -> StateMachine {
    let chasing = state::Chasing { target, speed };
    StateMachine::default()
        .trans::<AnyState, _>(trigger::zero_hp, state::Dead)
        .trans::<state::Idle, _>(trigger::near(target, 15.0), state::Firing)
        .trans::<state::Idle, _>(seldom_state::trigger::always, chasing)
        .trans::<state::Chasing, _>(trigger::near(target, 15.0), state::Firing)
        .on_enter::<state::Firing>(|e| {
            e.insert(WaitTimer::new(Duration::from_millis(500)));
        })
        .trans::<state::Firing, _>(trigger::wait_finished, state::Idle)
        .on_enter::<state::Chasing>(|e| {
            e.insert(SetAnimation::repeat("walk"));
        })
        .on_enter::<state::Firing>(|e| {
            e.insert(SetAnimation::repeat("fire"));
        })
        .on_enter::<state::Dead>(|e| {
            e.insert(SetAnimation::single("death"));
        })
}
