// um actually it's not REALLY an AI because it's not INTELLIGENT
mod ai;
mod command;
mod sniper;
mod spawner;

use std::time::Duration;

use bevy_common_assets::toml::TomlAssetPlugin;
use bevy_kira_audio::prelude::*;
use bevy_tweening::lens::SpriteColorLens;
use bevy_tweening::{Animator, EaseMethod, Tween};
use clap::ValueEnum;
use seldom_state::set::StateSet;
use serde::Deserialize;

use self::ai::state;
use self::sniper::spawn_reticles;
pub use self::spawner::EnemySpawner;
use self::spawner::{setup_spawner, spawn_enemies, SpawnAssets, WaveConfig};
use crate::prelude::*;
use crate::render::animation::Animations;
use crate::{
    actor::Enemy,
    damage::{ActualDamageEvent, HP},
    render::{
        animation::{AnimationFinished, AnimationSet, TRIGGER_DESPAWN},
        ScreenBounds,
    },
    scheduling::{GameState, GameTickSet},
};

/// Plugin for spawning enemies.
pub struct EnemyPlugin;

/// Exhaustive list of every single kind of enemy.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Deserialize, ValueEnum, Reflect)]
#[serde(rename_all = "snake_case")]
pub enum EnemyKind {
    Assault,
    Leader,
    Sniper,
}

/// All assets needed to spawn an enemy.
#[derive(AssetCollection, Resource)]
pub struct EnemyAssets {
    #[asset(texture_atlas(tile_size_x = 16., tile_size_y = 16., columns = 5, rows = 7))]
    layout: Handle<TextureAtlasLayout>,
    #[asset(path = "enemies/assault.png")]
    swarmer: Handle<Image>,
    #[asset(path = "enemies/leader.png")]
    leader: Handle<Image>,
    #[asset(path = "enemies/sniper.png")]
    sniper: Handle<Image>,
    #[asset(path = "enemies/shot.ogg")]
    shot_sfx: Handle<AudioSource>,
    #[asset(path = "enemies/sniper.wav")]
    snipe_sfx: Handle<AudioSource>,
    #[asset(path = "enemies/soldier.animation.toml")]
    animation: Handle<Animations>,
    #[asset(texture_atlas(tile_size_x = 9., tile_size_y = 9., columns = 2, rows = 1))]
    sniper_target_layout: Handle<TextureAtlasLayout>,
    #[asset(path = "fx/sniper_target.png")]
    sniper_target: Handle<Image>,
    #[asset(path = "fx/sniper_target.animation.toml")]
    sniper_target_animation: Handle<Animations>,
}

impl EnemyAssets {
    /// Get the texture atlas used for a given kind of enemy.
    fn layout(&self, kind: EnemyKind) -> Handle<TextureAtlasLayout> {
        use EnemyKind::*;
        match kind {
            Assault => self.layout.clone(),
            Leader => self.layout.clone(),
            Sniper => self.layout.clone(),
        }
    }

    fn texture(&self, kind: EnemyKind) -> Handle<Image> {
        use EnemyKind::*;
        match kind {
            Assault => self.swarmer.clone(),
            Leader => self.leader.clone(),
            Sniper => self.sniper.clone(),
        }
    }
}

impl Plugin for EnemyPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            Update,
            (
                (
                    spawn_enemies.run_if(resource_exists::<ScreenBounds>),
                    spawn_reticles,
                )
                    .run_if(in_state(GameState::Running)),
                death_animation_finished.after(AnimationSet::Animate),
            ),
        )
        .add_plugins(TomlAssetPlugin::<WaveConfig>::new(&["waves.toml"]))
        .fn_plugin(ai::ai_plugin)
        .configure_loading_state(
            LoadingStateConfig::new(GameState::Loading)
                .load_collection::<EnemyAssets>()
                .load_collection::<SpawnAssets>(),
        )
        .add_systems(
            PostUpdate,
            apply_red_to_hurt_enemies.after(GameTickSet::ApplyDamage),
        )
        .add_systems(OnEnter(GameState::Running), setup_spawner)
        .add_systems(Last, play_firing_sounds.after(StateSet::Transition));
    }
}

fn apply_red_to_hurt_enemies(
    mut commands: Commands,
    mut events: EventReader<ActualDamageEvent>,
    enemies: Query<&HP, With<Enemy>>,
) {
    for ActualDamageEvent { target, .. } in events.read() {
        if enemies.get(*target).map_or(false, |hp| hp.0 >= 0) {
            let tween = Tween::new(
                EaseMethod::Linear,
                Duration::from_millis(500),
                SpriteColorLens {
                    start: Color::RED,
                    end: Color::WHITE,
                },
            );
            commands.entity(*target).insert(Animator::new(tween));
        }
    }
}

fn death_animation_finished(
    mut commands: Commands,
    mut events: EventReader<AnimationFinished>,
    enemies: Query<(), With<Enemy>>,
) {
    for AnimationFinished { name, entity } in events.read() {
        if name == "death" && enemies.contains(*entity) {
            let tween = Tween::new(
                EaseMethod::Linear,
                Duration::from_millis(500),
                SpriteColorLens {
                    start: Color::WHITE,
                    end: Color::rgba(0., 0., 0., 0.),
                },
            )
            .with_completed_event(TRIGGER_DESPAWN);
            commands.entity(*entity).insert(Animator::new(tween));
        }
    }
}

/// Indicates that the enemy makes this noise when it fires.
#[derive(Debug, Clone, Component, PartialEq, Eq, Hash)]
pub struct FireAudio(pub Handle<AudioSource>);

/// Play all sounds that are associated with entities that fired.
fn play_firing_sounds(query: Query<&FireAudio, Added<state::Firing>>, audio: Res<Audio>) {
    // Only fire distinct sounds to avoid making it too loud.
    for sound in query.iter().unique() {
        audio
            .play(sound.0.clone())
            .with_volume(Volume::Amplitude(0.5));
    }
}
