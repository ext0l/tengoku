//! Defines building blocks for putting together enemy AI via [`seldom_state`]
//! state machines.
use std::time::Duration;

use bevy::time::TimeSystem;
use seldom_state::prelude::*;

use crate::{prelude::*, scheduling::GameTickSet};

pub(super) fn ai_plugin(app: &mut App) {
    app.add_plugins(StateMachinePlugin)
        .add_systems(PreUpdate, tick_wait_timers.after(TimeSystem))
        .add_systems(
            Update,
            (state::process_chase, state::process_lock_on).in_set(GameTickSet::SetVelocity),
        );
}

/// Internal struct used to automatically transition to another state after a
/// period of time.
#[derive(Clone, Component, Debug, Deref, DerefMut)]
pub struct WaitTimer(pub Timer);

impl WaitTimer {
    #[allow(dead_code)]
    pub fn new(duration: Duration) -> Self {
        Self(Timer::new(duration, TimerMode::Once))
    }
}

/// Ticks down all [`WaitTimer`]s.
fn tick_wait_timers(mut waits: Query<&mut WaitTimer>, time: Res<Time>) {
    for mut wait in &mut waits {
        wait.tick(time.delta());
    }
}

pub mod state {
    use super::*;

    /// Not doing anything in particular. In general, enemies should
    /// treat this as an 'initial' state they repeatedly come back to.
    #[derive(Copy, Clone, Reflect, Component)]
    #[component(storage = "SparseSet")]
    pub struct Idle;

    /// Move towards `target` with the given speed.
    #[derive(Copy, Clone, Reflect, Component)]
    #[component(storage = "SparseSet")]
    pub struct Chasing {
        pub target: Entity,
        pub speed: f32,
    }

    pub(super) fn process_chase(
        mut enemies: Query<(&Chasing, &GlobalTransform, &mut Velocity)>,
        transforms: Query<&GlobalTransform>,
    ) {
        for (chase, enemy, mut velocity) in &mut enemies {
            let Ok(target_transform) = transforms.get(chase.target) else {
                continue;
            };
            let displacement = target_transform.compute_transform().translation
                - enemy.compute_transform().translation;
            velocity.linvel = chase.speed * displacement.truncate().normalize_or_zero();
        }
    }

    /// The given entity's [`GlobalTransform`] is updated to be the same as the
    /// target's every frame. This assumes the two entities' coordinate frames
    /// are not scaled or rotated (but they can be translated).
    #[derive(Copy, Clone, Reflect, Component)]
    #[component(storage = "SparseSet")]
    pub struct LockedOn {
        pub target: Entity,
    }

    pub(super) fn process_lock_on(
        locked_on: Query<(&LockedOn, Entity)>,
        mut transforms: Query<&mut Transform>,
        global_transforms: Query<&GlobalTransform>,
    ) {
        for (LockedOn { target }, entity) in &locked_on {
            let offset = global_transforms.get(*target).unwrap().translation()
                - global_transforms.get(entity).unwrap().translation();
            transforms.get_mut(entity).unwrap().translation += offset;
        }
    }

    /// A "transient" state that's intended to cause the enemy to fire its
    /// weapon when entered. In general, this will immediately transition into
    /// [`Reloading`].
    #[derive(Clone, Copy, Reflect, Component)]
    pub struct Firing;

    /// Used to add some coooldown after an enemy fires.
    #[derive(Clone, Copy, Reflect, Component)]
    pub struct Reloading;

    /// This thing has hit zero HP and died. This is meant to be used as a sink
    /// state.
    #[derive(Clone, Copy, Reflect, Component)]
    pub struct Dead;
}

pub mod trigger {
    use super::*;
    use crate::{damage::HP, render::animation::AnimationState};

    /// Trigger that fires when the entity is within a given distance of the
    /// target. The distance must be nonnegative.
    #[derive(Copy, Clone, Debug, Reflect)]
    pub struct Near {
        pub target: Entity,
        pub distance: f32,
        /// If this is set, checks distance from this entity to the target
        /// instead.
        pub source: Option<Entity>,
    }

    pub fn near(target: Entity, threshold: f32) -> impl Trigger<Out = bool> {
        (move |In(us): In<Entity>, transforms: Query<&GlobalTransform>| {
            let us = transforms.get(us).unwrap().translation();
            let them = transforms.get(target).unwrap().translation();
            let distance = (us - them).truncate();
            distance.length() <= threshold
        })
        .into_trigger()
    }

    /// Triggers when the [`WaitTimer`] on this instance finishes.
    pub fn wait_finished(In(entity): In<Entity>, timers: Query<&WaitTimer>) -> bool {
        timers.get(entity).map_or(false, |t| t.just_finished())
    }

    /// Triggers when this has zero HP. Does nothing on entities that don't have
    /// HP or on things that are already dead.
    pub fn zero_hp(In(entity): In<Entity>, query: Query<&HP, Without<state::Dead>>) -> bool {
        query.get(entity).map_or(false, |hp| hp.0 <= 0)
    }

    /// Triggers when the entity has a child with the given component. The
    /// trigger data is the value of that component. It's valid to put this on
    /// an entity with no children; it will then just never trigger.
    pub fn has_child_with<T: Component + Clone>(
        In(entity): In<Entity>,
        has_component: Query<&T>,
        children: Query<&Children>,
    ) -> Option<T> {
        children
            .get(entity)
            .ok()?
            .iter()
            .find_map(|child| has_component.get(*child).ok())
            .cloned()
    }

    /// Triggers when the entity's [`AnimationState`] is finished.
    pub fn animation_finished(In(entity): In<Entity>, query: Query<&AnimationState>) -> bool {
        query
            .get(entity)
            .ok()
            .map_or(false, |state| !state.is_playing())
    }
}

#[cfg(test)]
mod tests {
    use bevy::time::TimeUpdateStrategy;

    use super::*;
    use crate::testing::{initialize_plugins, CommonTestPlugins};

    /// Calls `app.update`, using `duration` as the duration of the time tick.
    fn update_with_delta(app: &mut App, duration: Duration) {
        app.insert_resource(TimeUpdateStrategy::ManualDuration(duration));
        app.update();
    }

    #[test]
    fn test_waiting() {
        let mut app = App::new();
        app.fn_plugin(ai_plugin).add_plugins(CommonTestPlugins);
        let mut time = Time::default();
        time.update();
        app.world.insert_resource(time);

        // exists so we can run triggers for InitialState
        #[derive(Copy, Clone, Reflect, Component)]
        struct PreState;
        #[derive(Copy, Clone, Reflect, Component)]
        struct InitialState;
        #[derive(Copy, Clone, Reflect, Component)]
        struct NextState;
        #[derive(Copy, Clone, Reflect, Component)]
        struct FinalState;

        initialize_plugins(&mut app);

        // run startup systems
        app.update();

        let wait_duration = Duration::from_millis(100);
        let entity = app
            .world
            .spawn((
                StateMachine::default()
                    .trans::<PreState, _>(seldom_state::trigger::always, InitialState)
                    .trans::<InitialState, _>(trigger::wait_finished, NextState)
                    .trans::<NextState, _>(trigger::wait_finished, FinalState)
                    .on_enter::<InitialState>(move |e| {
                        e.insert(WaitTimer::new(wait_duration));
                    })
                    .on_enter::<NextState>(move |e| {
                        e.insert(WaitTimer::new(wait_duration));
                    })
                    .set_trans_logging(true),
                PreState,
            ))
            .id();
        app.update();
        assert!(
            app.world.get::<InitialState>(entity).is_some(),
            "world should contain the initial state"
        );

        update_with_delta(&mut app, wait_duration);
        assert!(
            app.world.get::<InitialState>(entity).is_none(),
            "world should not contain the initial state after waiting"
        );
        assert!(
            app.world.get::<NextState>(entity).is_some(),
            "world should contain the next state after waiting"
        );

        update_with_delta(&mut app, wait_duration / 2);
        assert!(
            app.world.get::<NextState>(entity).is_some(),
            "still in next state after waiting half the time"
        );

        update_with_delta(&mut app, wait_duration);
        assert!(
            app.world.get::<NextState>(entity).is_none(),
            "waiting the rest should move out of NextState"
        );
        assert!(
            app.world.get::<FinalState>(entity).is_some(),
            "world should be in the last state"
        );
    }

    #[test]
    fn test_has_child_with() {
        #[derive(Copy, Clone, Reflect, Component)]
        struct Before;
        #[derive(Copy, Clone, Reflect, Component)]
        struct After;
        #[derive(Copy, Clone, Reflect, Component)]
        struct Marker;
        let mut app = App::new();
        app.fn_plugin(ai_plugin).add_plugins(CommonTestPlugins);

        let entity = app
            .world
            .spawn((
                StateMachine::default()
                    .trans::<Before, _>(trigger::has_child_with::<Marker>, After),
                Before,
            ))
            .id();
        initialize_plugins(&mut app);

        app.update();
        assert!(
            app.world.get::<Before>(entity).is_some(),
            "HasChildWith should not fire if there is no child with the component"
        );

        app.world.entity_mut(entity).with_children(|build| {
            build.spawn(Marker);
        });
        app.update();
        app.update();
        assert!(
            app.world.get::<After>(entity).is_some(),
            "HasChildWith should fire when there's a child with the component"
        );
    }
}
