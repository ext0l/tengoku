use std::{collections::HashMap, time::Duration};

use bevy::prelude::*;
use bevy_asset_loader::prelude::*;
use bevy_rapier2d::prelude::*;
use rand::distributions::WeightedIndex;
use serde::Deserialize;
use serde_with::{serde_as, DurationSeconds};

use super::{command::SpawnEnemy, EnemyAssets, EnemyKind};
use crate::prelude::*;
use crate::{
    actor::{Enemy, Player},
    physics::{GROUP_ALLOW_SPAWN, GROUP_BLOCK_SPAWN, GROUP_SPAWN_CHECK},
    render::ScreenBounds,
    scheduling::RunningTime,
};

/// Describes an individual wave of enemies.
#[serde_as]
#[derive(Debug, Clone, Deserialize, Reflect)]
pub struct Wave {
    /// The amount of enemies to spawn each interval, by default.
    pub amount: usize,
    /// Amount of time between spawn checks. If not set, this is a one-shot
    /// spawn and will not repeat.
    #[serde_as(as = "Option<DurationSeconds>")]
    pub interval: Option<Duration>,
    /// After a spawn, we guarantee that there are at least this many enemies.
    pub min: usize,
    /// Don't spawn enemies if it would increase the bound past this many.
    pub max: usize,
    /// Chance to spawn each enemy. Doesn't need to add to 1.
    pub weights: HashMap<EnemyKind, f32>,
}

impl Wave {
    /// Get a random enemy kind to spawn, weighted by `self.weights`.
    pub fn sample_kind(&self) -> EnemyKind {
        let pairs: Vec<_> = self.weights.iter().collect();
        let dist = WeightedIndex::new(pairs.iter().map(|x| *x.1)).unwrap();
        *pairs[dist.sample(&mut thread_rng())].0
    }
}

/// Configures how enemies spawn.
#[derive(Debug, Clone, Resource, Deserialize, Reflect, Asset)]
#[serde(transparent)]
pub(super) struct WaveConfig(pub Wave);

impl Wave {
    /// Computes how many enemies we should spawn, given the current enemy
    /// count.
    fn amount_to_spawn(&self, current: usize) -> usize {
        self.amount.clamp(
            self.min.saturating_sub(current),
            self.max.saturating_sub(current),
        )
    }
}

/// Responsible for spawning enemies around the player.
#[derive(Component, Debug, Clone)]
pub struct EnemySpawner {
    /// If None, this is a one-shot spawner that will spawn enemies once and
    /// then remove itself.
    pub timer: Option<Timer>,
    pub wave: Wave,
}

impl EnemySpawner {
    /// A spawner that will spawn the given amount of enemies immediately and
    /// then despawn.
    pub fn one_shot(amount: usize, kind: EnemyKind) -> Self {
        Self {
            timer: None,
            wave: Wave {
                amount,
                interval: None,
                min: amount,
                // hacky hacky
                max: usize::MAX,
                weights: HashMap::from([(kind, 1.0)]),
            },
        }
    }
}

#[derive(Debug, Resource, AssetCollection)]
pub(super) struct SpawnAssets {
    #[asset(path = "map/spawns.waves.toml")]
    wave: Handle<WaveConfig>,
}

/// Ticks all [`EnemySpawner`] timers and spawns enemies for ones that fire.
#[allow(clippy::too_many_arguments)]
pub(super) fn spawn_enemies(
    mut commands: Commands,
    mut spawners: Query<(&mut EnemySpawner, Entity)>,
    _assets: Res<EnemyAssets>,
    enemies: Query<(), With<Enemy>>,
    time: Res<Time>,
    screen_bounds: Res<ScreenBounds>,
    _running_time: Res<RunningTime>,
    rapier: Res<RapierContext>,
    player: Query<Entity, With<Player>>,
) {
    // mut because we update it as we go
    let mut current_enemies = enemies.iter().len();
    let Ok(_player) = player.get_single() else {
        return;
    };
    for (mut spawner, entity) in &mut spawners {
        let should_spawn = if let Some(timer) = spawner.timer.as_mut() {
            timer.tick(time.delta());
            timer.just_finished()
        } else {
            commands.entity(entity).despawn_recursive();
            true
        };
        if should_spawn {
            let amount = spawner.wave.amount_to_spawn(current_enemies);
            for _ in 0..amount {
                if let Some(position) = accept_sampler(
                    || sample_position(screen_bounds.0, 5.0),
                    |pos| spawn_position_ok(&rapier, pos),
                ) {
                    let kind = spawner.wave.sample_kind();
                    commands.add(SpawnEnemy { kind, position });
                }
            }
            current_enemies += amount;
        }
    }
}

/// How many times do we try to spawn an enemy before we give up?
const SPAWN_ATTEMPTS: usize = 10;

/// Invoke `sample` until either `accept` returns true or we've tried too many
/// times.
fn accept_sampler(sample: impl Fn() -> Vec2, accept: impl Fn(Vec2) -> bool) -> Option<Vec2> {
    for _ in 0..SPAWN_ATTEMPTS {
        let pos = sample();
        if accept(pos) {
            return Some(pos);
        }
    }
    warn!("Failed to spawn an enemy after {SPAWN_ATTEMPTS} tries; giving up");
    None
}

/// Randomly pick a position offset by `offset` from the border of the rectangle
/// defined by `bounds`.
fn sample_position(bounds: Rect, offset: f32) -> Vec2 {
    // TODO: weigh this by aspect ratio or something
    let is_vertical = fastrand::bool();
    let sign = if fastrand::bool() { 1.0 } else { -1.0 };
    if is_vertical {
        // either above or below
        let x = bounds.min.x + fastrand::f32() * bounds.width();
        let y = bounds.center().y + sign * (bounds.half_size().y + offset);
        Vec2::new(x, y)
    } else {
        // either to the left or right
        let x = bounds.center().x + sign * (bounds.half_size().x + offset);
        let y = bounds.min.y + fastrand::f32() + bounds.height();
        Vec2::new(x, y)
    }
}

/// Whether it's OK to spawn an enemy at the given position.
fn spawn_position_ok(rapier: &Res<RapierContext>, position: Vec2) -> bool {
    let inside_spawn_zone = rapier
        .project_point(
            position,
            true,
            QueryFilter::default()
                .groups(CollisionGroups::new(GROUP_SPAWN_CHECK, GROUP_ALLOW_SPAWN)),
        )
        .map_or(false, |(_, proj)| proj.is_inside);
    let spawn_blocked = rapier
        .project_point(
            position,
            true,
            QueryFilter::default()
                .groups(CollisionGroups::new(GROUP_SPAWN_CHECK, GROUP_BLOCK_SPAWN)),
        )
        .map_or(false, |(_, proj)| proj.is_inside);
    inside_spawn_zone && !spawn_blocked
}

pub(super) fn setup_spawner(
    mut commands: Commands,
    assets: Res<SpawnAssets>,
    wave_configs: Res<Assets<WaveConfig>>,
) {
    let wave_config = wave_configs.get(&assets.wave).unwrap();
    let timer = wave_config
        .0
        .interval
        .map(|interval| Timer::new(interval, TimerMode::Repeating));
    commands.spawn(EnemySpawner {
        timer,
        wave: wave_config.0.clone(),
    });
}
