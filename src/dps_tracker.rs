use std::{collections::HashMap, time::Duration};

use bevy::prelude::*;

use crate::{
    damage::{ActualDamageEvent, DamageMethod},
    scheduling::GameTickSet,
    weapons::{Weapon, WeaponType},
};

/// Damage dealt and time that the entity has been present.
#[derive(Debug, Clone, Default)]
pub struct Dps {
    pub amount: u32,
    pub time: Duration,
}

impl Dps {
    /// Damage dealt over the time (in seconds). Returns None if the time is
    /// somehow zero.
    pub fn ratio(&self) -> Option<f32> {
        let secs = self.time.as_secs_f32();
        (secs != 0.0).then_some(self.amount as f32 / secs)
    }
}

/// Tracks an entity's DPS, keyed by the weapon type *and* the weapon's level.
/// We only track DPS dealt by weapons, since this is meant for the player and
/// the non-weapon damage methods are things like debug commands.
///
/// We could store this on the weapons themselves; we don't do this because if
/// we ever want to support deleting weapons, then we'd lose that DPS
/// information.
#[derive(Default, Debug, Clone, Component, Deref, DerefMut)]
pub struct DpsTracker(pub HashMap<(WeaponType, u32), Dps>);

/// Updates the `.time` field of each [`DPS`] in the tracker.
pub fn update_dps_tracker_time(
    mut trackers: Query<(&mut DpsTracker, &Children)>,
    weapons: Query<&Weapon>,
    time: Res<Time>,
) {
    let delta = time.delta();
    for (mut tracker, children) in &mut trackers {
        for weapon in children.iter().filter_map(|e| weapons.get(*e).ok()) {
            tracker
                .entry((weapon.kind, weapon.stats.level))
                .or_default()
                .time += delta;
        }
    }
}

/// Processes damage events and updates DPS trackers accordingly.
pub fn update_dps(mut query: Query<&mut DpsTracker>, mut events: EventReader<ActualDamageEvent>) {
    for event in events.read() {
        let Ok(mut tracker) = query.get_mut(event.source) else {
            continue;
        };
        let &DamageMethod::Weapon {
            kind: weapon,
            level,
        } = &event.method
        else {
            continue;
        };
        tracker.entry((weapon, level)).or_default().amount += event.total() as u32;
    }
}

pub fn dps_tracker_plugin(app: &mut App) {
    app.add_systems(
        PostUpdate,
        (update_dps_tracker_time, update_dps)
            .chain()
            .after(GameTickSet::ApplyDamage),
    );
}

#[cfg(test)]
mod tests {
    use bevy::{ecs::system::EntityCommand, time::TimeUpdateStrategy};
    use seldom_fn_plugin::FnPluginExt;

    pub use super::*;
    use crate::{
        damage::DamagePlugin,
        testing::CommonTestPlugins,
        weapons::{AddWeapon, AvailableWeapons},
    };

    /// Calls `app.update`, using `duration` as the duration of the time tick.
    fn update_with_delta(app: &mut App, duration: Duration) {
        app.insert_resource(TimeUpdateStrategy::ManualDuration(duration));
        app.update();
    }

    #[test]
    fn tracker_only_ticks_time_for_weapons_the_entity_has() {
        let mut app = App::new();
        app.add_plugins((CommonTestPlugins, DamagePlugin))
            .fn_plugin(dps_tracker_plugin);

        let before_shotgun = Duration::from_millis(300);
        update_with_delta(&mut app, before_shotgun);

        let player = app
            .world
            .spawn((DpsTracker::default(), AvailableWeapons::player_weapons()))
            .id();
        AddWeapon(WeaponType::Shotgun).apply(player, &mut app.world);
        let between_shotgun_and_laser = Duration::from_millis(400);
        update_with_delta(&mut app, between_shotgun_and_laser);

        AddWeapon(WeaponType::Laser).apply(player, &mut app.world);
        let after_laser = Duration::from_millis(500);
        update_with_delta(&mut app, after_laser);

        let tracker = app.world.get::<DpsTracker>(player).unwrap();
        assert_eq!(tracker[&(WeaponType::Laser, 0)].time, after_laser);
        assert_eq!(
            tracker[&(WeaponType::Shotgun, 0)].time,
            between_shotgun_and_laser + after_laser
        );
    }
}
