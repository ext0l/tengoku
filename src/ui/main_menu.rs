use std::sync::Arc;

use bevy::{app::AppExit, prelude::*};
use bevy_egui::{egui, EguiContexts};

use super::set_text_styles;
use crate::scheduling::{GameState, MainMenuSet};

fn ui_example_system(
    mut commands: Commands,
    mut contexts: EguiContexts,
    mut exit: EventWriter<AppExit>,
) {
    egui::CentralPanel::default().show(contexts.ctx_mut(), |ui| {
        set_text_styles(ui);

        ui.with_layout(egui::Layout::top_down(egui::Align::Center), |ui| {
            ui.label(
                egui::RichText::new("TENGOKU")
                    .font(egui::FontId::new(200.0, GameFont::Oxanium.into())),
            );

            if ui.add(egui::Button::new("EXECUTE")).clicked() {
                commands.insert_resource(NextState(Some(GameState::CreatingMap)));
            }

            if ui.add(egui::Button::new("DISCONNECT")).clicked() {
                exit.send(AppExit);
            }
        });

        ui.reset_style();
    });
}

fn setup_egui_font(mut contexts: EguiContexts) {
    let mut fonts = egui::FontDefinitions::default();
    // Unfortunately, we can't nicely use a loader to pull this in since bevy only
    // supports one asset loader per extension.
    fonts.font_data.insert(
        GameFont::Oxanium.into(),
        egui::FontData::from_static(include_bytes!("../../assets/ui/Oxanium-ExtraBold.ttf")),
    );
    fonts.font_data.insert(
        GameFont::Silver.into(),
        egui::FontData::from_static(include_bytes!("../../assets/ui/Silver.ttf")),
    );
    for font in GameFont::variants().into_iter() {
        fonts
            .families
            .insert(egui::FontFamily::Name(font.into()), vec![font.into()]);
    }
    // We don't set the styles here because that might interfere with inspectors
    // etc.
    let ctx = contexts.ctx_mut();
    ctx.set_fonts(fonts);
}

#[derive(Copy, Clone, Debug)]
enum GameFont {
    Oxanium,
    Silver,
}

impl GameFont {
    fn variants() -> [Self; 2] {
        [Self::Oxanium, Self::Silver]
    }
}

impl From<GameFont> for &str {
    fn from(value: GameFont) -> Self {
        match value {
            GameFont::Oxanium => "Oxanium",
            GameFont::Silver => "Silver",
        }
    }
}

impl From<GameFont> for String {
    fn from(value: GameFont) -> Self {
        let s: &str = value.into();
        s.to_owned()
    }
}

impl From<GameFont> for Arc<str> {
    fn from(value: GameFont) -> Self {
        let s: &str = value.into();
        s.into()
    }
}

impl From<GameFont> for egui::FontFamily {
    fn from(value: GameFont) -> Self {
        let s: &str = value.into();
        egui::FontFamily::Name(s.into())
    }
}

pub(super) fn main_menu_plugin(app: &mut App) {
    app.add_systems(Startup, setup_egui_font)
        .add_systems(Update, ui_example_system.in_set(MainMenuSet::MainMenu));
}
