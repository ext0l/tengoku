use bevy::ecs::system::{Command, EntityCommand};

use super::set_text_styles;
use crate::{
    actor::Player,
    player::xp::LevelUpEvent,
    prelude::*,
    scheduling::PauseConditionExt,
    weapons::{
        AddWeapon, AvailableWeapons, LevelBonus, UpgradeWeapon, Weapon, WeaponStats, WeaponType,
    },
};

/// Things the player can do when leveling up. To actually perform the level up,
/// just use this as an [`EntityCommand`].
#[derive(Debug, Clone)]
enum LevelUpCommand {
    AddWeapon(AddWeapon),
    UpgradeWeapon(UpgradeWeapon),
}

impl EntityCommand for LevelUpCommand {
    fn apply(self, entity: Entity, world: &mut World) {
        match self {
            LevelUpCommand::AddWeapon(inner) => inner.apply(entity, world),
            LevelUpCommand::UpgradeWeapon(inner) => inner.apply(entity, world),
        }
    }
}

/// Represents an item in the level-up menu. After the player picks this, send
/// it as a [`Command`].
#[derive(Debug, Clone)]
struct MenuItem {
    entity_command: LevelUpCommand,
    entity: Entity,
    title: String,
    description: String,
}

impl MenuItem {
    fn add_weapon(player: Entity, kind: WeaponType) -> Self {
        Self {
            entity_command: LevelUpCommand::AddWeapon(AddWeapon(kind)),
            entity: player,
            title: format!("{kind:?}"),
            description: kind.description().into(),
        }
    }

    fn upgrade_weapon(entity: Entity, weapon: &Weapon) -> Self {
        Self {
            entity_command: LevelUpCommand::UpgradeWeapon(UpgradeWeapon),
            entity,
            title: format!("Upgrade {:?}", weapon.kind),
            description: format_stat_change(&weapon.stats),
        }
    }
}

impl Command for MenuItem {
    fn apply(self, world: &mut World) {
        self.entity_command.with_entity(self.entity).apply(world)
    }
}

/// How many choices does the player have when leveling up?
#[derive(Debug, Resource)]
struct LevelUpChoiceCount(pub usize);

#[derive(Debug, Resource)]
struct LevelUpMenuItems(Vec<MenuItem>);

impl LevelUpMenuItems {
    /// Gather all the possible level up options for the player. Panics if the
    /// player does not exist.
    fn initialize(
        available: Query<(Entity, &AvailableWeapons), With<Player>>,
        level_up_weapons: Query<(Entity, &Weapon)>,
        mut commands: Commands,
        choice_count: Res<LevelUpChoiceCount>,
    ) {
        let (player, available) = available.single();
        let add_weapon = available
            .0
            .iter()
            .map(|&kind| MenuItem::add_weapon(player, kind));

        let level_up_weapon = level_up_weapons
            .iter()
            .filter(|(_, weap)| !weap.stats.is_max_level())
            .map(|(weapon, weap)| MenuItem::upgrade_weapon(weapon, weap));

        let mut menu_items: Vec<MenuItem> = add_weapon.chain(level_up_weapon).collect();
        fastrand::shuffle(&mut menu_items);
        menu_items.truncate(choice_count.0);
        commands.insert_resource(Self(menu_items));
    }

    /// Remove this resource. Doing this ensures that we can't accidentally
    /// access stale level up options.
    fn clear(commands: &mut Commands) {
        commands.remove_resource::<Self>();
    }
}

/// Formats the stat change that would be obtained from leveling this up.
fn format_stat_change(stats: &WeaponStats) -> String {
    use LevelBonus::*;
    let bonus = &stats.level_bonus[stats.level as usize];
    match bonus {
        Damage(damage) => format!("Damage: {} -> {}", stats.damage, stats.damage + damage),
        Cooldown(cooldown) => format!(
            "Cooldown: {}ms -> {}ms",
            (stats.cooldown.as_millis()),
            (stats.cooldown - *cooldown).as_millis()
        ),
        Amount(amount) => format!("Amount: {} -> {}", stats.amount, stats.amount + amount),
    }
}

/// Shows the level-up button for the given option.
fn level_up_button(ui: &mut egui::Ui, option: &MenuItem) -> egui::Response {
    let button = egui::Button::new(format!("{}\n{}", option.title, option.description));
    ui.add(button)
}

fn level_up_menu(
    mut commands: Commands,
    mut contexts: EguiContexts,
    options: Res<LevelUpMenuItems>,
) {
    let ctx = contexts.ctx_mut();
    let center = ctx.screen_rect().center();
    egui::Window::new("level up")
        .pivot(egui::Align2::CENTER_CENTER)
        .title_bar(false)
        .auto_sized()
        .default_pos(center)
        .show(ctx, |ui| {
            set_text_styles(ui);

            ui.label("Level up!");

            if options.0.is_empty() && ui.add(egui::Button::new("Nothing to do :(")).clicked() {
                LevelUpMenuItems::clear(&mut commands);
            }

            for option in options.0.iter() {
                let button = level_up_button(ui, option);
                if button.clicked() {
                    commands.add(option.clone());
                    LevelUpMenuItems::clear(&mut commands);
                }
            }
        });
}

pub(super) fn level_up_plugin(app: &mut App) {
    app.insert_resource(LevelUpChoiceCount(3))
        .add_systems(
            Update,
            (
                LevelUpMenuItems::initialize.run_if(on_event::<LevelUpEvent>()),
                level_up_menu.run_if(resource_exists::<LevelUpMenuItems>),
            )
                .chain(),
        )
        .pause_if("Level up menu", |res: Option<Res<LevelUpMenuItems>>| {
            res.is_some()
        });
}
