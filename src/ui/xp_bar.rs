use bevy::{prelude::*, ui::FocusPolicy};

use crate::{
    player::xp::Xp,
    scheduling::{GameState, UiSet},
};

/// Marker component for the component of the XP bar that fills up as the player
/// gains XP.
#[derive(Component)]
pub struct XpBarFilledPart;

fn spawn_xp_bar(mut commands: Commands) {
    commands
        .spawn((
            NodeBundle {
                style: Style {
                    position_type: PositionType::Absolute,
                    bottom: (Val::Px(0.0)),
                    padding: UiRect::all(Val::Px(5.0)),
                    width: Val::Percent(100.0),
                    height: Val::Px(30.0),
                    ..default()
                },
                background_color: Color::BLACK.with_a(0.8).into(),
                focus_policy: FocusPolicy::Block,
                ..default()
            },
            Name::new("XP bar container"),
        ))
        .with_children(|container| {
            container.spawn((
                NodeBundle {
                    style: Style {
                        width: Val::Percent(0.0),
                        height: Val::Percent(100.0),
                        ..default()
                    },
                    background_color: Color::rgb_u8(50, 102, 160).into(),
                    ..default()
                },
                XpBarFilledPart,
            ));
        });
}

fn update_xp_bar(
    query: Query<&Xp, Changed<Xp>>,
    mut bars: Query<&mut Style, With<XpBarFilledPart>>,
) {
    let Ok(xp) = query.get_single() else {
        return;
    };
    for mut style in &mut bars {
        style.as_mut().width = Val::Percent(xp.ratio() * 100.0);
    }
}

pub(super) fn xp_plugin(app: &mut App) {
    app.add_systems(OnEnter(GameState::Running), spawn_xp_bar)
        .add_systems(Update, update_xp_bar.in_set(UiSet));
}
