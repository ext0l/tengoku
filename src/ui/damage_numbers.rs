use std::time::Duration;

use extol_pixel_font::{PixelFont, PixelFontBundle, PixelFontText};

fn spawn_damage_numbers(
    mut commands: Commands,
    damage_font: Res<DamageFont>,
    mut events: EventReader<ActualDamageEvent>,
    transforms: Query<&GlobalTransform>,
) {
    for event in events.read() {
        if event.total() == 0 {
            continue;
        }
        let Ok(transform) = transforms.get(event.target) else {
            continue;
        };
        let transform = Transform::from_translation(transform.translation());
        commands.spawn((
            Name::new(format!("Damage number [{}]", event.total())),
            DespawnTimer::new(Duration::from_secs(1)),
            PixelFontBundle {
                text: PixelFontText {
                    text: format!("{}", event.total()),
                    font: damage_font.0.clone(),
                    font_height: None,
                },
                transform,
                ..default()
            },
            ZLayer::Ui,
            Velocity::linear(Vec2::new(0.0, 10.0)),
            RigidBody::KinematicVelocityBased,
        ));
    }
}

use crate::{
    damage::ActualDamageEvent, despawn_timer::DespawnTimer, prelude::*, render::ZLayer,
    scheduling::GameState,
};
#[derive(AssetCollection, Resource)]
struct DamageNumberAssets {
    #[asset(texture_atlas(tile_size_x = 5., tile_size_y = 12., columns = 20, rows = 5))]
    layout: Handle<TextureAtlasLayout>,
    #[asset(path = "ui/damage_font.png")]
    texture: Handle<Image>,
}

#[derive(Clone, Deref, Resource)]
struct DamageFont(Handle<PixelFont>);

impl FromWorld for DamageFont {
    fn from_world(world: &mut World) -> Self {
        let cell = world.cell();
        let assets = cell.get_resource::<DamageNumberAssets>().unwrap();
        let mut pixel_fonts = cell.get_resource_mut::<Assets<PixelFont>>().unwrap();
        let s = r##"
 !"#$%&'()*+,-./0123
456789:;<=>?@ABCDEFG
HIJKLMNOPQRSTUVWXYZ[
\]^_`abcdefghijklmno
pqrstuvwxyz{|}~
"##;
        let pixel_font = pixel_fonts.add(PixelFont::new(
            assets.layout.clone(),
            assets.texture.clone(),
            s,
        ));
        Self(pixel_font)
    }
}

/// Renders damage numbers whenever something takes damage.
pub(super) fn damage_numbers_plugin(app: &mut App) {
    app.configure_loading_state(
        LoadingStateConfig::new(GameState::Loading)
            .load_collection::<DamageNumberAssets>()
            .init_resource::<DamageFont>(),
    )
    .add_systems(
        Update,
        spawn_damage_numbers.run_if(in_state(GameState::Running)),
    );
}
