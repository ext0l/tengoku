use crate::{
    actor::Player,
    dps_tracker::DpsTracker,
    prelude::*,
    scheduling::{GameState, UiSet},
};

/// Marker for the root UI element that
#[derive(Component)]
struct DpsTable;

fn render_dps_table(mut contexts: EguiContexts, dps: Query<&DpsTracker, With<Player>>) {
    let ctx = contexts.ctx_mut();
    let Ok(dps) = dps.get_single() else {
        return;
    };
    let mut sorted_dps: Vec<_> = dps.iter().collect();
    sorted_dps.sort_unstable_by(|a, b| a.1.ratio().partial_cmp(&b.1.ratio()).unwrap());
    egui::Window::new("dps table")
        .pivot(egui::Align2::RIGHT_BOTTOM)
        .title_bar(false)
        .auto_sized()
        .default_pos(ctx.screen_rect().right_bottom())
        .show(ctx, |ui| {
            egui::Grid::new("dps_grid").show(ui, |ui| {
                for (weapon, dps) in &sorted_dps {
                    let Some(ratio) = dps.ratio() else {
                        continue;
                    };
                    ui.label(format!("{:?}", weapon));
                    ui.label(format!("{:.1}", ratio));
                    ui.end_row();
                }
            });
        });
}

pub(super) fn dps_table_plugin(app: &mut App) {
    app.add_systems(
        Update,
        render_dps_table
            .run_if(in_state(GameState::Running))
            .in_set(UiSet),
    );
}
