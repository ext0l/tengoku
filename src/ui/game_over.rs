use bevy::prelude::*;

use super::fonts::FontAssets;
use crate::{
    actor::Player,
    damage::HP,
    scheduling::{GameState, GameTickSet},
};

/// If the player is at zero HP, move to the 'game over' state.
fn check_game_over(mut commands: Commands, query: Query<&HP, With<Player>>) {
    for hp in &query {
        if hp.0 <= 0 {
            commands.insert_resource(NextState(Some(GameState::GameOver)));
        }
    }
}

fn show_game_over_screen(mut commands: Commands, assets: Res<FontAssets>) {
    commands
        .spawn(NodeBundle {
            // darken the game's UI
            style: Style {
                left: Val::Percent(0.0),
                right: Val::Percent(100.0),
                top: Val::Percent(0.0),
                bottom: Val::Percent(100.0),
                width: Val::Percent(100.0),
                height: Val::Percent(100.0),
                flex_direction: FlexDirection::Column,
                align_items: AlignItems::Center,
                justify_content: JustifyContent::Center,
                ..default()
            },
            background_color: BackgroundColor(Color::rgba(0.0, 0.0, 0.0, 0.8)),
            ..default()
        })
        .with_children(|parent| {
            parent.spawn(TextBundle::from_section(
                "GAME OVER",
                TextStyle {
                    font: assets.silver.clone(),
                    font_size: 128.0,
                    color: Color::WHITE,
                },
            ));
        });
}

pub(super) fn game_over_plugin(app: &mut App) {
    app.add_systems(
        Last,
        check_game_over
            .in_set(GameTickSet::Cleanup)
            .run_if(in_state(GameState::Running)),
    )
    .add_systems(OnEnter(GameState::GameOver), show_game_over_screen);
}
