use std::time::Duration;

use bevy::prelude::*;
use bevy_tweening::{lens::UiPositionLens, Animator, Delay, EaseFunction, Tween};

use super::fonts::FontAssets;
use crate::{
    render::animation::TRIGGER_DESPAWN,
    scheduling::{GameState, UiSet},
};

/// Marker for the container for the event log lines.
#[derive(Component)]
pub struct HudLogContainer {
    style: TextStyle,
}

impl HudLogContainer {
    fn log_entry_animator(&self) -> Animator<Style> {
        let tween = Delay::new(Duration::from_secs(5)).then(
            Tween::new(
                EaseFunction::CubicIn,
                Duration::from_millis(800),
                UiPositionLens {
                    start: UiRect::left(Val::Px(0.0)),
                    end: UiRect::left(Val::Px(-500.0)),
                },
            )
            .with_completed_event(TRIGGER_DESPAWN),
        );
        Animator::new(tween)
    }
}

/// Send an event of this type, and it will be shown to the user in a corner of
/// the screen.
#[derive(Debug, Event)]
pub struct HudLogEvent {
    pub message: String,
}

/// Spawn the container for the HUD log events in the bottom-right.
fn spawn_hud_log_container(mut commands: Commands, fonts: Res<FontAssets>) {
    commands.spawn((
        HudLogContainer {
            style: TextStyle {
                font_size: 32.0,
                color: Color::WHITE,
                font: fonts.silver.clone(),
            },
        },
        NodeBundle {
            background_color: Color::BLACK.with_a(0.8).into(),
            style: Style {
                position_type: PositionType::Absolute,
                left: Val::Px(0.0),
                bottom: Val::Px(0.0),
                flex_direction: FlexDirection::Column,
                align_items: AlignItems::Start,
                justify_content: JustifyContent::End,
                ..default()
            },
            ..default()
        },
    ));
}

fn display_events(
    mut commands: Commands,
    mut reader: EventReader<HudLogEvent>,
    query: Query<(Entity, &HudLogContainer)>,
) {
    for event in reader.read() {
        for (entity, container) in &query {
            commands.entity(entity).with_children(|parent| {
                parent.spawn((
                    TextBundle {
                        text: Text::from_section(&event.message, container.style.clone()),
                        ..default()
                    },
                    container.log_entry_animator(),
                ));
            });
        }
    }
}

pub(super) fn hud_log_plugin(app: &mut App) {
    app.add_event::<HudLogEvent>()
        .add_systems(OnEnter(GameState::Running), spawn_hud_log_container)
        .add_systems(Update, display_events.in_set(UiSet));
}
