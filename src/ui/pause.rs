use bevy::{input::common_conditions::input_just_pressed, prelude::*};

use crate::scheduling::{GameState, PauseConditionExt};

#[derive(Clone, Debug, Default, Resource)]
struct PauseMenu;

/// Unpauses in the pause menu and vice versa.
fn toggle_pause(state: Option<Res<PauseMenu>>, mut commands: Commands) {
    if state.is_some() {
        debug!("unpausing");
        commands.remove_resource::<PauseMenu>();
    } else {
        debug!("pausing");
        commands.init_resource::<PauseMenu>();
    }
}

pub(super) fn pause_plugin(app: &mut App) {
    app.add_systems(
        Update,
        toggle_pause
            .run_if(in_state(GameState::Running))
            .run_if(input_just_pressed(KeyCode::Escape)),
    )
    .pause_if("Pause menu", |res: Option<Res<PauseMenu>>| res.is_some());
}
