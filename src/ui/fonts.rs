use crate::prelude::*;

/// Asset collection for fonts. We store these in one place because a lot of
/// systems need fonts, and it makes no sense to have each one load the font
/// assets independently.
#[derive(AssetCollection, Resource)]
pub struct FontAssets {
    #[asset(path = "ui/Silver.ttf")]
    pub silver: Handle<Font>,
}
