mod damage_numbers;
mod dps_table;
pub mod fonts;
mod game_over;
pub mod hud_log;
mod level_up;
mod main_menu;
mod pause;
mod xp_bar;

use bevy::prelude::*;
use bevy_asset_loader::prelude::*;
use bevy_egui::egui;
use seldom_fn_plugin::FnPluginExt;

use crate::scheduling::GameState;

/// Set the default text styles. We only do this when we start drawing the UI so
/// that we don't mess with the inspector.
fn set_text_styles(ui: &mut egui::Ui) {
    ui.style_mut()
        .text_styles
        .insert(egui::TextStyle::Body, egui::FontId::proportional(36.0));
    ui.style_mut()
        .text_styles
        .insert(egui::TextStyle::Button, egui::FontId::proportional(36.0));
}

pub fn ui_plugin(app: &mut App) {
    app.configure_loading_state(
        LoadingStateConfig::new(GameState::Loading).load_collection::<fonts::FontAssets>(),
    )
    .fn_plugin(game_over::game_over_plugin)
    .fn_plugin(hud_log::hud_log_plugin)
    .fn_plugin(xp_bar::xp_plugin)
    .fn_plugin(main_menu::main_menu_plugin)
    .fn_plugin(pause::pause_plugin)
    .fn_plugin(level_up::level_up_plugin)
    .fn_plugin(dps_table::dps_table_plugin)
    .fn_plugin(damage_numbers::damage_numbers_plugin);
}
