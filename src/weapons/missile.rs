use std::{f32::consts::PI, time::Duration};

use bevy::{ecs::query::QueryEntityError, prelude::*};
use bevy_asset_loader::prelude::*;
use bevy_kira_audio::prelude::*;
use bevy_rapier2d::prelude::*;

use super::{targeting::random_target, FireQueryItem, WeaponParams, WeaponStats, WeaponType};
use crate::{
    damage::{DamageOnHit, DamageSource},
    despawn_timer::DespawnTimer,
    physics::{GROUP_ENEMY_HURTBOX, GROUP_PLAYER_HITBOX},
    render::{
        animation::{AnimationMode, AnimationState, Animations},
        ZLayer,
    },
    scheduling::DespawnDuringCleanup,
    weapons::LevelBonus,
};

/// The missile will find a random target within this radius.
const MISSILE_TARGET_RADIUS: f32 = 140.0;
/// Amount the missile accelerates per second.
const MISSILE_ACCEL: f32 = 8000.0;
/// Without acceleration, the missile takes this long to halve its velocity.
const MISSILE_DAMPING_HALF_LIFE: Duration = Duration::from_millis(40);
/// Amount of time it takes for the missile to start accelerating.
const MISSILE_ACCEL_DELAY: Duration = Duration::from_millis(10);
/// How fast is the missile when it launches?
const MISSILE_LAUNCH_SPEED: f32 = 1000.0;

pub(super) fn stats() -> WeaponStats {
    use LevelBonus::*;
    WeaponStats {
        level: 0,
        damage: 40,
        // how many missiles are fired at once
        amount: 1,
        duration: Some(Duration::from_millis(4000)),
        cooldown: Duration::from_millis(2000),
        level_bonus: vec![
            Cooldown(Duration::from_millis(400)),
            Amount(1),
            Cooldown(Duration::from_millis(400)),
            Cooldown(Duration::from_millis(200)),
            Amount(1),
        ],
    }
}

pub(super) fn fire<'a>(
    commands: &'a mut Commands,
    stats: &'a WeaponStats,
    item: &'a FireQueryItem,
    params: &'a WeaponParams,
) {
    let WeaponParams {
        assets,
        audio,
        rapier,
        enemy_query,
    } = params;
    let fire_pos = item.transform.translation().truncate();

    for _ in 0..stats.amount {
        let Some((target, target_pos)) =
            random_target(rapier, fire_pos, MISSILE_TARGET_RADIUS, enemy_query)
        else {
            return;
        };

        let launch_velocity =
            MISSILE_LAUNCH_SPEED * launch_vector(target_pos, fire_pos) + item.velocity.linvel;

        commands.spawn((
            SpriteSheetBundle {
                atlas: TextureAtlas {
                    layout: assets.missile.layout.clone(),
                    index: 2,
                },
                texture: assets.missile.texture.clone(),
                transform: Transform::from_translation(fire_pos.extend(0.0)),
                ..default()
            },
            ZLayer::Projectile,
            Velocity::linear(launch_velocity),
            RigidBody::KinematicVelocityBased,
            Missile {
                accel_timer: Timer::new(MISSILE_ACCEL_DELAY, TimerMode::Once),
                target,
                target_pos,
            },
            DespawnTimer::new(stats.duration.unwrap()),
            // We use this so that the missile explosion can know how much damage to do. This
            // doesn't have a collider, so it can't hit anything (since it's notionally
            // above the ground).
            DamageOnHit::from_weapon_stats(WeaponType::Missile, stats),
            DamageSource(item.player),
        ));
    }

    audio
        .play(assets.missile.sound.clone())
        .with_volume(Volume::Amplitude(0.4));
}

/// If launching a vector from `target_pos` to `player_pos`, find the vector for
/// the initial missile to launch along. Returned vector is guaranteed to be
/// normalized or zero.
fn launch_vector(target_pos: Vec2, player_pos: Vec2) -> Vec2 {
    let perp = (target_pos - player_pos).perp().normalize_or_zero();
    // Launch to the left or the right of the target.
    let vector = if fastrand::bool() { perp } else { -perp };
    // Randomly 'wiggle' it a bit.
    let random_angle = PI / 4.0 * (fastrand::f32() - 0.5);
    Vec2::from_angle(random_angle).rotate(vector)
}

#[derive(Component, Debug)]
pub(super) struct Missile {
    accel_timer: Timer,
    target: Entity,
    /// Target position. We store this separately so that if the target dies we
    /// can keep moving towards where it was.
    target_pos: Vec2,
}

pub(super) fn missile_velocities(
    time: Res<Time>,
    positions: Query<&GlobalTransform>,
    mut query: Query<(
        &mut Missile,
        &mut Transform,
        &GlobalTransform,
        &mut Velocity,
    )>,
) {
    for (mut missile, mut transform, global_transform, mut velocity) in &mut query {
        missile.accel_timer.tick(time.delta());
        match positions.get(missile.target) {
            Ok(target_pos) => {
                missile.target_pos = target_pos.translation().truncate();
            }
            Err(QueryEntityError::NoSuchEntity(_)) => (),
            Err(err) => panic!("bad error when getting missile position: {:?}", err),
        }
        let displacement = missile.target_pos - global_transform.translation().truncate();
        let scale =
            0.5_f32.powf(time.delta().as_secs_f32() / MISSILE_DAMPING_HALF_LIFE.as_secs_f32());
        velocity.linvel *= scale;
        if missile.accel_timer.finished() {
            velocity.linvel +=
                time.delta().as_secs_f32() * displacement.normalize_or_zero() * MISSILE_ACCEL;
        }
        transform.rotation = Quat::from_rotation_z(Vec2::X.angle_between(velocity.linvel));
    }
}

pub(super) fn detonate_missiles(
    mut commands: Commands,
    query: Query<(
        Entity,
        &Missile,
        &GlobalTransform,
        &DamageOnHit,
        &DamageSource,
    )>,
    assets: Res<MissileAssets>,
) {
    for (entity, missile, transform, damage, source) in &query {
        let pos = transform.translation().truncate();
        if (pos - missile.target_pos).length() > 8.0 {
            continue;
        }
        // boom!

        commands
            .spawn((
                SpriteSheetBundle {
                    atlas: TextureAtlas {
                        layout: assets.explosion_layout.clone(),
                        index: 0,
                    },
                    texture: assets.explosion_texture.clone(),
                    transform: Transform::from_translation(transform.translation()),
                    ..default()
                },
                ZLayer::Fx,
                AnimationState::new(
                    "explode",
                    AnimationMode::Single,
                    assets.explosion_animation.clone(),
                ),
                // XXX: don't hard-code the duration!
                DespawnTimer::new(Duration::from_millis(900)),
            ))
            .with_children(|parent| {
                let physics = (
                    RigidBody::Dynamic,
                    LockedAxes::ROTATION_LOCKED,
                    Sensor,
                    CollidingEntities::default(),
                    Collider::cuboid(10.0, 10.0),
                    CollisionGroups::new(GROUP_PLAYER_HITBOX, GROUP_ENEMY_HURTBOX),
                );
                parent.spawn((
                    physics,
                    damage.clone(),
                    DespawnDuringCleanup,
                    SpatialBundle::default(),
                    source.clone(),
                ));
            });

        commands.entity(entity).despawn_recursive();
    }
}

#[derive(AssetCollection, Resource)]
pub(super) struct MissileAssets {
    #[asset(texture_atlas(tile_size_x = 16., tile_size_y = 16., columns = 3, rows = 1))]
    layout: Handle<TextureAtlasLayout>,
    #[asset(path = "weapons/rpg-round.png")]
    texture: Handle<Image>,
    #[asset(path = "weapons/rocket-launcher.ogg")]
    sound: Handle<AudioSource>,
    #[asset(texture_atlas(tile_size_x = 24., tile_size_y = 24., columns = 9, rows = 1))]
    explosion_layout: Handle<TextureAtlasLayout>,
    #[asset(path = "fx/small-explosion.png")]
    explosion_texture: Handle<Image>,
    #[asset(path = "fx/small-explosion.animation.toml")]
    explosion_animation: Handle<Animations>,
}
