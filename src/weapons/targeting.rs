//! Utility functions for weapon target acquisition.
use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::{
    actor::Enemy,
    physics::{GROUP_ENEMY_HURTBOX, GROUP_PLAYER_HITBOX},
};

/// Find a random target within the given radius. Anything with an 'enemy
/// hurtbox' collision group is a potential target.
pub fn random_target(
    rapier: &RapierContext,
    player_pos: Vec2,
    radius: f32,
    enemies: &Query<&GlobalTransform, With<Enemy>>,
) -> Option<(Entity, Vec2)> {
    let mut targets: Vec<(Entity, Vec2)> = vec![];
    rapier.intersections_with_shape(
        player_pos,
        0.0,
        &Collider::ball(radius),
        QueryFilter::new().groups(CollisionGroups::new(
            GROUP_PLAYER_HITBOX,
            GROUP_ENEMY_HURTBOX,
        )),
        |entity| {
            if let Some(pos) = rapier
                .collider_parent(entity)
                .and_then(|parent| enemies.get(parent).ok())
            {
                targets.push((entity, pos.translation().truncate()));
            }
            true
        },
    );
    if targets.is_empty() {
        None
    } else {
        Some(targets[fastrand::usize(0..targets.len())])
    }
}
