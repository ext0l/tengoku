//! Defines [`WeaponsPlugin`] for managing weapon spawning.

mod laser;
mod missile;
mod shotgun;
mod stats;
mod targeting;

use std::collections::HashSet;

use bevy::{
    ecs::{
        query::QueryData,
        system::{EntityCommand, SystemParam},
    },
    prelude::*,
};
use bevy_asset_loader::prelude::*;
use bevy_kira_audio::prelude::*;
use bevy_rapier2d::prelude::*;

pub use self::stats::*;
use self::{
    laser::LaserAssets,
    missile::{detonate_missiles, missile_velocities, MissileAssets},
    shotgun::ShotgunAssets,
};
use crate::{
    actor::Enemy,
    damage::{DamageOnHit, DamageSource},
    despawn_timer::DespawnTimerPlugin,
    ensure_plugin,
    render::ZLayer,
    scheduling::{GameState, GameTickSet},
};

/// The direction the entity is currently aiming at. Some weapons use this to
/// determine firing direction.
#[derive(Component, Deref, Debug, Clone)]
pub struct Aim(pub Vec2);

impl Aim {
    /// Sets the aim to point along the given vector, which doesn't need to be
    /// normalized. Setting it to a zero-ish value will do nothing.
    pub fn set(&mut self, vec: Vec2) {
        if let Some(normal) = vec.try_normalize() {
            self.0 = normal
        }
    }
}

/// Indicates that the entity is automatically aiming at something.
#[derive(Component, Deref, Debug, Clone)]
pub struct AimingAt(pub Entity);

/// Sets the [`Aim`] of every component with an [`AimingAt`].
fn set_aim_from_aiming_at(
    mut aim: Query<(&mut Aim, &AimingAt, &GlobalTransform)>,
    transform: Query<&GlobalTransform>,
) {
    for (mut aim, &AimingAt(target), our_transform) in &mut aim {
        let displacement =
            transform.get(target).unwrap().translation() - our_transform.translation();
        aim.set(displacement.truncate());
    }
}

#[derive(SystemParam)]
struct WeaponParams<'w, 's> {
    assets: WeaponAssets<'w>,
    audio: Res<'w, Audio>,
    rapier: Res<'w, RapierContext>,
    enemy_query: Query<'w, 's, &'static GlobalTransform, With<Enemy>>,
}

/// Information about the entity firing a weapon that's passed to the firing
/// functions.
#[derive(QueryData, Debug)]
struct FireQuery {
    pub player: Entity,
    /// Allows for projectile velocity inheritance.
    pub velocity: &'static Velocity,
    pub transform: &'static GlobalTransform,
    pub aim: &'static Aim,
}

impl<'w> FireQueryItem<'w> {
    /// A vector from this to that. Either normalized or zero.
    fn towards(&self, pos: Vec2) -> Vec2 {
        (pos - self.transform.translation().truncate()).normalize_or_zero()
    }
}

/// The weapons that the player can have, but doesn't yet own. This omits enemy
/// weapons and weapons that are 'implementation details'.
#[derive(Component, Clone, Default, Debug, Deref, DerefMut)]
pub struct AvailableWeapons(pub HashSet<WeaponType>);

impl AvailableWeapons {
    pub fn player_weapons() -> Self {
        Self(HashSet::from([
            WeaponType::Laser,
            WeaponType::Missile,
            WeaponType::Shotgun,
        ]))
    }
}

type WeaponFireFn = fn(&mut Commands, &WeaponStats, &FireQueryItem, &WeaponParams);

#[derive(Component, Reflect)]
#[reflect(from_reflect = false)]
pub struct Weapon {
    #[reflect(ignore)]
    fire: WeaponFireFn,
    pub stats: WeaponStats,
    pub kind: WeaponType,
}

impl From<WeaponType> for Weapon {
    fn from(kind: WeaponType) -> Self {
        use WeaponType::*;
        let stats = WeaponStats::for_weapon(kind);
        let fire = match kind {
            Shotgun => shotgun::fire,
            Laser => laser::fire,
            Missile => missile::fire,
        };
        Self { fire, stats, kind }
    }
}

#[derive(Clone, Debug, Component, Deref, DerefMut)]
pub struct WeaponTimer(pub Timer);

fn fire_weapons(
    mut commands: Commands,
    fire_query: Query<FireQuery>,
    mut weapons: Query<(&Weapon, &mut WeaponTimer, &Parent)>,
    params: WeaponParams,
    time: Res<Time>,
) {
    for (weapon, mut timer, parent) in &mut weapons {
        timer.tick(time.delta());
        if !timer.just_finished() {
            continue;
        }
        let fire_item = fire_query.get(**parent).unwrap();
        (weapon.fire)(&mut commands, &weapon.stats, &fire_item, &params);
    }
}

#[derive(Debug, Clone)]
pub struct AddWeapon(pub WeaponType);

impl EntityCommand for AddWeapon {
    fn apply(self, id: Entity, world: &mut World) {
        let mut available = world.get_mut::<AvailableWeapons>(id).unwrap();
        let kind = self.0;
        assert!(
            available.remove(&kind),
            "tried to add a weapon {kind:?} that wasn't available"
        );
        let weapon = Weapon::from(kind);
        world.entity_mut(id).with_children(|build| {
            build.spawn((
                Weapon::from(kind),
                WeaponTimer(Timer::new(weapon.stats.cooldown, TimerMode::Repeating)),
                Name::new(format!("{:?}", kind)),
            ));
        });
    }
}

#[derive(Debug, Clone)]
pub struct UpgradeWeapon;

impl EntityCommand for UpgradeWeapon {
    fn apply(self, id: Entity, world: &mut World) {
        world.get_mut::<Weapon>(id).unwrap().stats.level_up();
    }
}

/// All the assets used for firing weapons. We stick them all on one struct so
/// that we don't have to worry about having one resource for each possible
/// weapon type.
#[derive(SystemParam)]
struct WeaponAssets<'w> {
    shotgun: Res<'w, ShotgunAssets>,
    laser: Res<'w, LaserAssets>,
    missile: Res<'w, MissileAssets>,
}

/// Bundle to describe things that are hitbox-y: i.e., on collision with
/// something, they damage it.
#[derive(Bundle)]
pub struct HitboxBundle {
    damage: DamageOnHit,
    source: DamageSource,
    groups: CollisionGroups,
    collider: Collider,
    colliding: CollidingEntities,
    events: ActiveEvents,
    rigid_body: RigidBody,
}

impl HitboxBundle {
    pub fn new(
        damage: DamageOnHit,
        source: DamageSource,
        collider: Collider,
        groups: CollisionGroups,
    ) -> Self {
        Self {
            damage,
            source,
            collider,
            groups,
            colliding: CollidingEntities::default(),
            events: ActiveEvents::COLLISION_EVENTS,
            rigid_body: RigidBody::Dynamic,
        }
    }
}

/// Bundle to describe projectiles: things that a player or enemy fires that
/// damage things on hit and moves.
#[derive(Bundle)]
pub struct ProjectileBundle {
    hitbox: HitboxBundle,
    velocity: Velocity,
    layer: ZLayer,
    locked_axes: LockedAxes,
    sensor: Sensor,
}

impl ProjectileBundle {
    pub fn new(velocity: Velocity, hitbox: HitboxBundle) -> Self {
        Self {
            hitbox,
            velocity,
            layer: ZLayer::Projectile,
            locked_axes: LockedAxes::ROTATION_LOCKED,
            sensor: Sensor,
        }
    }
}

/// Plugin that manages spawning the player's weapons.
pub struct WeaponsPlugin;

impl Plugin for WeaponsPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            Update,
            (
                (fire_weapons, detonate_missiles, set_aim_from_aiming_at)
                    .run_if(in_state(GameState::Running)),
                missile_velocities.in_set(GameTickSet::SetVelocity),
            ),
        )
        .configure_loading_state(
            LoadingStateConfig::new(GameState::Loading)
                .load_collection::<MissileAssets>()
                .load_collection::<LaserAssets>()
                .load_collection::<ShotgunAssets>(),
        )
        .register_type::<WeaponType>();
        ensure_plugin(app, DespawnTimerPlugin);
    }
}
