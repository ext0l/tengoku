/// Data about weapon stats. This is purely data (i.e., no systems or anything).
use std::time::Duration;

use bevy::prelude::*;

/// All the possible weapons in the game.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Component, Reflect)]
pub enum WeaponType {
    Shotgun,
    Laser,
    Missile,
}

impl WeaponType {
    pub fn description(self) -> &'static str {
        use WeaponType::*;
        match self {
            Shotgun => "Fires a pellet spread in the direction you're aiming.",
            Laser => "Targets the nearest enemy. Infinite pierce.",
            Missile => "Targets a random enemy with splash damage.",
        }
    }
}

#[derive(Debug, Reflect)]
/// Data about a weapon.
pub struct WeaponStats {
    /// Levels start at *zero*, because this is a game about robots.
    pub level: u32,
    pub damage: u32,
    /// The exact meaning of this is weapon-dependent; generally this causes it
    /// to fire off more at once.
    pub amount: u32,
    pub duration: Option<Duration>,
    pub cooldown: Duration,
    /// `level_bonus[i]` is awarded when going from level i to i + 1
    pub level_bonus: Vec<LevelBonus>,
}

impl WeaponStats {
    pub fn level_up(&mut self) {
        let bonus = self
            .level_bonus
            .get(self.level as usize)
            .unwrap_or_else(|| {
                panic!(
                    "tried to level up, but level {} >= max level {}",
                    self.level,
                    self.max_level()
                )
            });
        self.level += 1;
        match bonus {
            LevelBonus::Damage(damage) => self.damage += damage,
            LevelBonus::Cooldown(cooldown) => self.cooldown -= *cooldown,
            LevelBonus::Amount(amount) => self.amount += amount,
        }
    }

    pub fn is_max_level(&self) -> bool {
        // use >= in case we somehow wind up over max level
        self.level >= self.max_level()
    }

    pub fn max_level(&self) -> u32 {
        self.level_bonus.len() as u32
    }

    pub fn for_weapon(weapon: WeaponType) -> Self {
        use WeaponType::*;
        match weapon {
            Shotgun => super::shotgun::stats(),
            Laser => super::laser::stats(),
            Missile => super::missile::stats(),
        }
    }
}

/// Indicates how to increase the stats of a given weapon.
#[derive(Debug, Reflect)]
pub enum LevelBonus {
    /// Damage increases by the given amount.
    Damage(u32),
    /// Cooldown *decreases* by the given amount.
    Cooldown(Duration),
    /// Fires more of whatever this is. The meaning is weapon-dependent.
    Amount(u32),
}
