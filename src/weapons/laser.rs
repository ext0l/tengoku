use std::time::Duration;

use bevy::prelude::*;
use bevy_asset_loader::prelude::*;
use bevy_kira_audio::prelude::*;
use bevy_rapier2d::prelude::*;
use bevy_tweening::lens::SpriteColorLens;
use bevy_tweening::{Animator, Delay, EaseMethod, Tween};

use super::targeting::random_target;
use super::{FireQueryItem, HitboxBundle, LevelBonus, WeaponParams, WeaponStats, WeaponType};
use crate::damage::{DamageOnHit, DamageSource};
use crate::despawn_timer::DespawnTimer;
use crate::physics::{GROUP_ENEMY_HURTBOX, GROUP_PLAYER_HITBOX};
use crate::render::animation::TRIGGER_DESPAWN;
use crate::render::ZLayer;

const LASER_LENGTH: f32 = 200.;
const LASER_WIDTH: f32 = 4.0;
const LASER_OFFSET: f32 = 14.0;
/// Amount of time the laser's sprite takes to fade out after it stops being
/// active.
const FADE_DURATION: Duration = Duration::from_millis(300);

pub(super) fn stats() -> WeaponStats {
    use LevelBonus::*;
    WeaponStats {
        level: 0,
        damage: 2,
        amount: 1,
        level_bonus: vec![
            Damage(2),
            Cooldown(Duration::from_millis(400)),
            Amount(1),
            Cooldown(Duration::from_millis(400)),
            Damage(1),
        ],
        cooldown: Duration::from_millis(2400),
        // How long the laser is active
        duration: Some(Duration::from_millis(200)),
    }
}

pub(super) fn fire<'a>(
    commands: &'a mut Commands,
    stats: &'a WeaponStats,
    item: &'a FireQueryItem,
    params: &'a WeaponParams,
) {
    let WeaponParams {
        assets,
        audio,
        rapier,
        enemy_query,
    } = params;
    let fire_pos = item.transform.translation().truncate();

    for _ in 0..stats.amount {
        let Some((_, enemy_pos)) = random_target(rapier, fire_pos, 400.0, enemy_query) else {
            return;
        };
        let towards_enemy = item.towards(enemy_pos);
        let duration = stats.duration.unwrap();

        // Place where the 'muzzle flash' is spawned.
        let flash_point = towards_enemy * 10.0;
        // Find the midpoint of the laser beam, since bevy and rapier work off
        // midpoints.
        let midpoint = towards_enemy * (LASER_OFFSET + LASER_LENGTH / 2.0);

        let flash_tween = Delay::new(duration).then(
            Tween::new(
                EaseMethod::Linear,
                FADE_DURATION,
                SpriteColorLens {
                    start: Color::WHITE,
                    end: Color::NONE,
                },
            )
            .with_completed_event(TRIGGER_DESPAWN),
        );

        let laser_tween = Delay::new(duration).then(
            Tween::new(
                EaseMethod::Linear,
                FADE_DURATION,
                SpriteColorLens {
                    start: Color::WHITE,
                    end: Color::NONE,
                },
            )
            // XXX: magic constant, etc
            .with_completed_event(TRIGGER_DESPAWN),
        );

        // Spawn the laser and the
        commands.entity(item.player).with_children(|parent| {
            parent.spawn((
                Name::from("Laser flash"),
                SpriteBundle {
                    texture: assets.laser.laser_flash.clone(),
                    transform: Transform::from_translation(flash_point.extend(0.0)),
                    ..default()
                },
                ZLayer::Projectile,
                Animator::new(flash_tween),
            ));

            // This contains the laser's sprite; we spawn the collider separately below so
            // we can remove the collider while we do the 'fade-out' effect.
            parent
                .spawn((
                    Name::from("Laser sprite"),
                    SpriteBundle {
                        sprite: Sprite {
                            custom_size: Some(Vec2::new(LASER_LENGTH, LASER_WIDTH)),
                            ..default()
                        },
                        texture: assets.laser.laser_sprite.clone(),
                        transform: Transform::from_translation(midpoint.extend(0.)).with_rotation(
                            Quat::from_rotation_z(Vec2::X.angle_between(towards_enemy)),
                        ),
                        ..default()
                    },
                    ZLayer::Projectile,
                    Animator::new(laser_tween),
                ))
                .with_children(|laser| {
                    laser.spawn((
                        Name::from("Laser hitbox"),
                        HitboxBundle::new(
                            DamageOnHit::from_weapon_stats(WeaponType::Laser, stats),
                            DamageSource(item.player),
                            Collider::cuboid(LASER_LENGTH / 2.0, LASER_WIDTH / 2.0),
                            CollisionGroups::new(GROUP_PLAYER_HITBOX, GROUP_ENEMY_HURTBOX),
                        ),
                        TransformBundle::default(),
                        DespawnTimer::new(duration),
                        Sensor,
                    ));
                });
        });
    }

    audio
        .play(assets.laser.laser_sound.clone())
        .with_volume(Volume::Amplitude(0.9));
}

#[derive(AssetCollection, Resource, Default)]
pub(super) struct LaserAssets {
    #[asset(path = "weapons/laser.png")]
    laser_sprite: Handle<Image>,
    #[asset(path = "weapons/laser.ogg")]
    laser_sound: Handle<AudioSource>,
    #[asset(path = "weapons/laser-flash.png")]
    laser_flash: Handle<Image>,
}
