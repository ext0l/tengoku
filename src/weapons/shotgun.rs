use std::{f32::consts::PI, time::Duration};

use bevy::{prelude::*, sprite::Anchor};
use bevy_asset_loader::prelude::*;
use bevy_kira_audio::prelude::*;
use bevy_rapier2d::prelude::*;

use super::{
    FireQueryItem, HitboxBundle, LevelBonus, ProjectileBundle, WeaponParams, WeaponStats,
    WeaponType,
};
use crate::{
    damage::{DamageOnHit, DamageSource},
    despawn_timer::DespawnTimer,
    physics::{DestroyOnCollision, GROUP_ENEMY_HURTBOX, GROUP_PLAYER_HITBOX},
    render::trails::GhostTrail,
};

pub(super) fn stats() -> WeaponStats {
    use LevelBonus::*;
    WeaponStats {
        level: 0,
        damage: 20,
        // number of pellets; increasing this also increases spread
        amount: 3,
        level_bonus: vec![
            Damage(5),
            Cooldown(Duration::from_millis(200)),
            Amount(6),
            Cooldown(Duration::from_millis(200)),
            Amount(4),
            Damage(15),
            Cooldown(Duration::from_millis(200)),
        ],
        cooldown: Duration::from_millis(1500),
        duration: Some(Duration::from_millis(1000)),
    }
}

const SPREAD_PER_BULLET: f32 = PI / 8.;

pub(super) fn fire<'a>(
    commands: &'a mut Commands,
    stats: &'a WeaponStats,
    item: &'a FireQueryItem,
    params: &'a WeaponParams,
) {
    let WeaponParams { assets, audio, .. } = params;
    let fire_pos = item.transform.translation().truncate();
    let towards_enemy = item.aim.0;
    let spread = SPREAD_PER_BULLET * (stats.amount as f32).sqrt();
    for i in 0..stats.amount {
        let angle = Vec2::X.angle_between(towards_enemy) - spread / 2.
            + spread * (i as f32) / (stats.amount as f32 - 1.);
        let position = fire_pos + 3. * Vec2::from_angle(angle);
        let velocity = 400.0 * Vec2::from_angle(angle);
        let sprite = SpriteBundle {
            sprite: Sprite {
                color: Color::WHITE,
                custom_size: Some(Vec2::new(2.0, 2.0)),
                anchor: Anchor::Center,
                ..default()
            },
            transform: Transform::from_translation(position.extend(0.))
                .with_rotation(Quat::from_rotation_z(angle)),
            ..default()
        };

        commands.spawn((
            sprite,
            ProjectileBundle::new(
                Velocity::linear(velocity),
                HitboxBundle::new(
                    DamageOnHit::from_weapon_stats(WeaponType::Shotgun, stats),
                    DamageSource(item.player),
                    Collider::cuboid(2., 2.),
                    CollisionGroups::new(GROUP_PLAYER_HITBOX, GROUP_ENEMY_HURTBOX),
                ),
            ),
            DespawnTimer::new(stats.duration.unwrap()),
            DestroyOnCollision,
            GhostTrail {
                duration: Duration::from_millis(100),
                height: 2.0,
            },
        ));
    }

    audio
        .play(assets.shotgun.shotgun_sound.clone())
        .with_volume(Volume::Amplitude(0.5));
}

#[derive(AssetCollection, Resource)]
pub(super) struct ShotgunAssets {
    #[asset(path = "weapons/shotgun.ogg")]
    shotgun_sound: Handle<AudioSource>,
}
