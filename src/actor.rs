use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::damage::HP;

/// Marker component indicating that something is the player.
#[derive(Component, Debug)]
pub struct Player;

pub struct PlayerParams {
    pub actor: ActorParams,
    pub hp: i32,
}

impl PlayerParams {
    pub fn into_bundle(self) -> impl Bundle {
        (self.actor.into_bundle(), HP(self.hp), Player)
    }
}

/// Marker component indicating that something is an enemy. This should only be
/// used for actual enemies, not 'hostile' entities (projectiles fired by
/// enemies, etc). May need faction marker components for that later.
#[derive(Component, Debug)]
pub struct Enemy;

/// Sets up an actor to participate in physics.
pub struct ActorParams {
    pub collider: Collider,
}

impl ActorParams {
    pub fn into_bundle(self) -> impl Bundle {
        (
            RigidBody::Dynamic,
            LockedAxes::ROTATION_LOCKED,
            Velocity::zero(),
            Damping {
                linear_damping: 6.0,
                angular_damping: 6.0,
            },
            CollidingEntities::default(),
            // This is necessary; otherwise CollidingEntities doesn't get populated.
            ActiveEvents::COLLISION_EVENTS,
            self.collider,
        )
    }
}
