use std::time::Duration;

use bevy::sprite::Anchor;
use bevy_tweening::{lens::SpriteColorLens, Animator, EaseMethod, Tween};

use super::{animation::TRIGGER_DESPAWN, ZLayer};
use crate::prelude::*;

/// Indicates that the entity should spawn a 'ghost trail' as it moves. The
/// trail will be gray-ish and fade out over time. This is useful for things
/// like bullet trails etc. The current implementation relies on entities
/// moving using `Velocity`.
#[derive(Debug, Component)]
pub struct GhostTrail {
    /// Duration the ghost trail will take to fade.
    pub duration: Duration,
    /// Height for the ghost trails. This should be close-ish to the height of
    /// the entity.
    pub height: f32,
}

/// Spawns the ghost trail for every entity.
pub(super) fn spawn_ghost_trails(
    mut commands: Commands,
    query: Query<(&GhostTrail, &GlobalTransform, &Velocity)>,
    time: Res<Time>,
) {
    for (&GhostTrail { duration, height }, global_transform, velocity) in &query {
        let start = Color::rgba(1.0, 1.0, 1.0, 0.1);
        let end = start.with_a(0.0);
        let mut transform = global_transform.compute_transform();
        let displacement = velocity.linvel * time.delta().as_secs_f32();
        transform.translation -= displacement.extend(0.) / 2.0;
        let width = displacement.length();
        let tween = Tween::new(EaseMethod::Linear, duration, SpriteColorLens { start, end })
            .with_completed_event(TRIGGER_DESPAWN);
        commands.spawn((
            Name::new("Ghost trail"),
            SpriteBundle {
                sprite: Sprite {
                    color: Color::WHITE,
                    custom_size: Some(Vec2::new(width, height)),
                    anchor: Anchor::Center,
                    ..default()
                },
                // since we've already run transform propagation, we need to explicitly set the
                // global transform here
                global_transform: GlobalTransform::from(transform),
                transform,
                ..default()
            },
            ZLayer::Fx,
            Animator::new(tween),
        ));
    }
}
