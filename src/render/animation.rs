//! Systems for animating sprites. This is broadly inspired by `micro_banimate`,
//! but rewritten to be more in line with my design sensibilities. Maybe one day
//! I'll spin this off into my own crate.
use std::{collections::HashMap, time::Duration};

use bevy::prelude::*;
use bevy_common_assets::toml::TomlAssetPlugin;
use bevy_tweening::TweenCompleted;
use serde::Deserialize;
use serde_with::{serde_as, DurationMilliSeconds};

use crate::scheduling::{DespawnDuringCleanup, GameTickSet};

/// The number of the frame in the originating atlas. This is a wrapper type so
/// we can't get it confused with a `usize` representing the index in the frames
/// list, and since doing math on this is generally not what you want.
#[derive(Deserialize, Debug, Clone, Copy, PartialEq, Eq, Reflect)]
#[serde(transparent)]
pub struct Frame(pub usize);

#[derive(Deserialize, Debug, Clone, PartialEq, Eq, Reflect, Asset)]
#[serde(transparent)]
pub struct Animations(pub HashMap<String, Animation>);

#[serde_as]
#[derive(Deserialize, Debug, Clone, PartialEq, Eq, Reflect)]
pub struct Animation {
    /// The (zero-indexed) frames for the animation. Repeating frames is valid.
    pub frames: Vec<Frame>,
    /// Duration for each frame. Per-frame durations are not yet supported; one
    /// workaround is to repeat some of your frame IDs.
    #[serde_as(as = "DurationMilliSeconds")]
    pub frame_time: Duration,
}

/// The current state of something being animated: what animation is playing,
/// and how far in it are we?
#[derive(Component, Debug, Reflect)]
pub struct AnimationState {
    /// Currently-playing animation. This must always be a valid key in the
    /// [`AnimationSet`] this points to.
    name: String,
    /// Index into the `Vec<Frame>`. This is *not* the index in the texture
    /// atlas! If you want to know the index in the texture atlas, look at the
    /// `index` field on the [`TextureAtlasSprite`] component.
    frame_index: usize,
    /// Whether this animation is actually playing. Automatically set to false
    /// when a one-shot animation finishes.
    pub playing: bool,
    /// Adjusts how the animation is playing. You can mutate this to your
    /// heart's content. Note that if using `AnimationMode::RepeatTimes`, this
    /// will get decremented each loop through the animation, so it won't store
    /// the original number of times requested.
    pub mode: AnimationMode,
    /// Time we've spent in the current frame. We count time *up* because that
    /// way you don't need to know frame timing information to construct one of
    /// these.
    time_in_current_frame: Duration,
    /// Current animation set.
    animations: Handle<Animations>,
}

/// An event fired whenever an animation finishes. Since this only triggers when
/// an event *finishes*, it only fires if the mode is `AnimationMode::Single`.
/// It also does not fire if an animation is cancelled.
#[derive(Debug, Clone, PartialEq, Eq, Event)]
pub struct AnimationFinished {
    pub name: String,
    pub entity: Entity,
}

/// Insert this on an animated entity to set its animation. This is intended to
/// be an easy bridge with our `seldom_state`-based AI, which has the ability to
/// insert a bundle on state transition. This is automatically cleared each
/// frame.
#[derive(Clone, Debug, Component)]
#[component(storage = "SparseSet")]
pub struct SetAnimation {
    pub name: String,
    pub mode: AnimationMode,
}

impl SetAnimation {
    pub fn single(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            mode: AnimationMode::Single,
        }
    }

    pub fn repeat(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            mode: AnimationMode::RepeatForever,
        }
    }
}

/// Animation set for systems related to animation.
#[derive(Clone, PartialEq, Eq, Debug, Hash, SystemSet)]
pub enum AnimationSet {
    /// All animation is done in this set. This also includes animations from
    /// `bevy_tweening`.
    Animate,
}

impl AnimationState {
    pub fn new(
        name: impl Into<String>,
        mode: AnimationMode,
        animation_set: Handle<Animations>,
    ) -> Self {
        Self {
            name: name.into(),
            frame_index: 0,
            mode,
            playing: true,
            time_in_current_frame: Duration::ZERO,
            animations: animation_set,
        }
    }

    pub fn is_playing(&self) -> bool {
        self.playing
    }

    /// Starts playing the given animation. If that animation was already
    /// playing, does not restart it.
    pub fn play(&mut self, name: impl Into<String>, mode: AnimationMode) {
        let name: String = name.into();
        self.playing = true;
        if self.name != name {
            self.name = name;
            self.mode = mode;
            self.restart();
        }
    }

    /// Restarts the currently-playing animation.
    pub fn restart(&mut self) {
        self.frame_index = 0;
        self.time_in_current_frame = Duration::ZERO;
    }

    /// Ticks the animation forward.
    fn tick(&mut self, tick: Duration, animation: &Animation) -> DidAnimationFinish {
        self.time_in_current_frame += tick;
        while self.time_in_current_frame >= animation.frame_time {
            self.time_in_current_frame -= animation.frame_time;
            match self.mode {
                AnimationMode::Single => {
                    if self.frame_index == animation.frames.len() - 1 {
                        return DidAnimationFinish::Yes;
                    } else {
                        self.frame_index += 1;
                        return DidAnimationFinish::No;
                    }
                }
                AnimationMode::RepeatForever => {
                    self.frame_index = (self.frame_index + 1) % animation.frames.len()
                }
            }
        }
        DidAnimationFinish::No
    }
}

/// Returned from `AnimationState::Tick` to indicate whether or not the
/// animation finished.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum DidAnimationFinish {
    Yes,
    No,
}

/// Controls whether this animation plays once, loops, etc.
#[derive(Debug, Clone, Default, Reflect)]
pub enum AnimationMode {
    #[default]
    Single,
    RepeatForever,
}

/// When used as a value for `.with_completed_event`, causes the entity to be
/// (recursively) despawned.
pub const TRIGGER_DESPAWN: u64 = 0xDEADCAFE;

/// Panics if there are any animated entities that don't have a
/// [`TextureAtlas`] (since then there's nothing to animate!).
fn ensure_animations_have_sprites(
    query: Query<Entity, (With<AnimationState>, Without<TextureAtlas>)>,
) {
    if let Some(entity) = query.iter().next() {
        panic!("{entity:?} has an AnimationState but no TextureAtlas");
    }
}

fn set_animations(
    mut commands: Commands,
    mut states: Query<(Entity, &mut AnimationState, &SetAnimation)>,
) {
    for (entity, mut state, SetAnimation { name, mode }) in &mut states {
        state.play(name.clone(), mode.clone());
        state.restart();
        commands.entity(entity).remove::<SetAnimation>();
    }
}

fn update_animations(
    mut states: Query<(Entity, &mut AnimationState, &mut TextureAtlas)>,
    sets: Res<Assets<Animations>>,
    time: Res<Time>,
    mut events: EventWriter<AnimationFinished>,
) {
    for (entity, mut state, mut sprite) in &mut states {
        if !state.playing {
            continue;
        }
        let animation_set = sets
            .get(&state.animations)
            .expect("Bad AnimationSet handle");
        let animation = animation_set
            .0
            .get(&state.name)
            .unwrap_or_else(|| panic!("animation '{}' should be present", &state.name));
        if state.tick(time.delta(), animation) == DidAnimationFinish::Yes {
            state.playing = false;
            events.send(AnimationFinished {
                name: state.name.clone(),
                entity,
            });
        }
        sprite.index = animation.frames[state.frame_index].0;
    }
}

fn process_tween_completion_events(
    mut commands: Commands,
    mut events: EventReader<TweenCompleted>,
) {
    for &TweenCompleted { entity, user_data } in events.read() {
        match user_data {
            TRIGGER_DESPAWN => {
                // The entity might have despawned for other reasons.
                if let Some(mut cmds) = commands.get_entity(entity) {
                    cmds.insert(DespawnDuringCleanup);
                }
            }
            _ => error!("Unknown event completion data {user_data}"),
        }
    }
}

/// Main plugin entrypoint for animations.
pub struct SlideshowAnimationPlugin;

impl Plugin for SlideshowAnimationPlugin {
    fn build(&self, app: &mut App) {
        app.configure_sets(Update, AnimationSet::Animate)
            .configure_sets(Update, bevy_tweening::AnimationSystem::AnimationUpdate)
            .add_systems(
                Update,
                (
                    ensure_animations_have_sprites,
                    set_animations,
                    update_animations.after(GameTickSet::SetVelocity),
                    process_tween_completion_events
                        .after(bevy_tweening::AnimationSystem::AnimationUpdate),
                )
                    .chain()
                    .in_set(AnimationSet::Animate),
            )
            .add_plugins(TomlAssetPlugin::<Animations>::new(&["animation.toml"]))
            .add_event::<AnimationFinished>()
            .register_type::<AnimationState>();
    }
}
