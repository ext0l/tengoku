pub mod animation;
mod auto_flip;
pub mod distortion;
mod postprocess;
pub mod trails;

pub use auto_flip::AutoFlip;
use bevy::prelude::*;
use bevy::sprite::MaterialMesh2dBundle;
use bevy::{
    render::{
        camera::RenderTarget,
        render_resource::{
            Extent3d, TextureDescriptor, TextureDimension, TextureFormat, TextureUsages,
        },
        view::RenderLayers,
    },
    transform::TransformSystem,
    window::WindowResized,
};
use bevy_rapier2d::prelude::PhysicsSet;
use extol_sprite_layer::LayerIndex;
use seldom_fn_plugin::FnPluginExt;

use self::auto_flip::set_flip_x;
use self::postprocess::PostprocessMaterial;
use crate::actor::Player;
use crate::scheduling::{GameState, GameTickSet, Paused};

/// Consistent z-indices for layering things on top of each other.
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Component, Hash, Reflect)]
pub enum ZLayer {
    /// The map. Beneath everything else.
    #[allow(dead_code)]
    Map,
    Enemy,
    Fx,
    Projectile,
    /// The player. This is always on top of everything (other than UI).
    Player,
    /// The few things that need to go above the player but below the UI.
    AbovePlayer,
    Ui,
}

impl LayerIndex for ZLayer {
    fn as_z_coordinate(&self) -> f32 {
        match *self {
            ZLayer::Map => 0.,
            // due to bevy_ecs_tilemap internals, the map effectively has a z-extent of 2.0
            ZLayer::Enemy => 2.,
            ZLayer::Fx => 3.,
            ZLayer::Projectile => 4.,
            // 1000. is the clipping plane, so 998 is the largest we can get.
            ZLayer::Player => 996.,
            ZLayer::AbovePlayer => 997.,
            ZLayer::Ui => 998.,
        }
    }
}

/// Marks the camera that tracks the player and renders to the intermediate
/// texture.
#[derive(Component)]
struct PlayerCamera;

/// Marks the camera that renders the texture and upscales it to fit the window.
#[derive(Component)]
struct OuterCamera;

/// Marks the mesh that we render the game screen into for postprocessing.
#[derive(Component)]
struct VirtualScreen;

/// The worldspace bounds of what the player can currently see on screen. This
/// is used to, e.g., make sure enemies only spawn offscreen.
#[derive(Debug, Resource)]
pub struct ScreenBounds(pub Rect);

/// Number of 'logical pixels' to render at.
pub(super) const INNER_WIDTH: u32 = 320;
pub(super) const INNER_HEIGHT: u32 = 240;
/// Subpixel resolution to render on. A value of 2 means that everything renders
/// at half-pixel precision.
pub const SUBPIXELS: u32 = 2;

fn spawn_camera(
    mut commands: Commands,
    mut images: ResMut<Assets<Image>>,
    mut postprocess_materials: ResMut<Assets<PostprocessMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
    windows: Query<&Window>,
) {
    let size = Extent3d {
        width: INNER_WIDTH * SUBPIXELS,
        height: INNER_HEIGHT * SUBPIXELS,
        ..default()
    };

    let window = windows.single();

    // We use this as a render target for the game world, then upscale it to ensure
    // that everything is pixel-accurate.
    let mut virtual_screen = Image {
        texture_descriptor: TextureDescriptor {
            label: None,
            size,
            dimension: TextureDimension::D2,
            format: TextureFormat::Bgra8UnormSrgb,
            mip_level_count: 1,
            sample_count: 1,
            usage: TextureUsages::TEXTURE_BINDING
                | TextureUsages::COPY_DST
                | TextureUsages::RENDER_ATTACHMENT,
            view_formats: &[],
        },
        ..default()
    };

    // Inititialize the image's size and zero out its data buffer.
    virtual_screen.resize(size);

    let image_handle = images.add(virtual_screen);

    // This camera renders to the canvas target; it exists 'in the game world'.
    commands.spawn((
        Name::new("Player camera"),
        Camera2dBundle {
            projection: OrthographicProjection {
                near: -1000.0,
                scale: 1. / SUBPIXELS as f32,
                ..default()
            },
            camera: Camera {
                order: -1,
                target: RenderTarget::Image(image_handle.clone()),
                ..default()
            },
            ..default()
        },
        PlayerCamera,
        // don't render the UI this way
        // XXX: what do we do here now?
        // UiCameraConfig { show_ui: false },
    ));

    // Fortunately, since the virtual screen image is always a fixed size anyway, we
    // don't have to worry about resizind this if the game window resizes.
    let virtual_screen_mesh = meshes.add(Mesh::from(Rectangle::new(
        size.width as f32,
        size.height as f32,
    )));

    let postprocess_material = postprocess_materials.add(PostprocessMaterial::new(image_handle));

    // Put the virtual screen mesh and its camera into the world.
    commands.spawn((
        Name::new("Virtual screen"),
        MaterialMesh2dBundle {
            mesh: virtual_screen_mesh.into(),
            material: postprocess_material,
            ..default()
        },
        RenderLayers::layer(1),
        VirtualScreen,
    ));
    commands.spawn((
        Name::new("Outer camera"),
        Camera2dBundle {
            projection: OrthographicProjection {
                near: -1000.0,
                scale: compute_scale(window.width(), window.height()),
                ..default()
            },
            ..default()
        },
        RenderLayers::layer(1),
        OuterCamera,
    ));
}

fn compute_scale(width: f32, height: f32) -> f32 {
    let h_scale = width / (INNER_WIDTH * SUBPIXELS) as f32;
    let v_scale = height / (INNER_HEIGHT * SUBPIXELS) as f32;
    1. / h_scale.max(v_scale).ceil()
}

fn rescale_outer_camera(
    mut resize_event: EventReader<WindowResized>,
    mut q: Query<&mut OrthographicProjection, With<OuterCamera>>,
) {
    let mut projection = q.single_mut();
    for event in resize_event.read() {
        projection.scale = compute_scale(event.width, event.height);
    }
}

/// Makes the [`PlayerCamera`] track the player.
fn track_player(
    player_transform: Query<&Transform, (With<Player>, Without<PlayerCamera>)>,
    mut camera_transform: Query<&mut Transform, (With<PlayerCamera>, Without<Player>)>,
) {
    let Ok(player) = player_transform.get_single() else {
        return;
    };
    let player_position = player.translation;

    for mut transform in &mut camera_transform {
        transform.translation.x = player_position.x + 0.1;
        transform.translation.y = player_position.y + 0.1;
    }
}

/// Updates the [`ScreenBounds`] resource from the player and camera position.
fn update_screen_bounds(
    mut commands: Commands,
    player_transform: Query<&Transform, With<Player>>,
    camera: Query<&OrthographicProjection, With<OuterCamera>>,
) {
    let Ok(transform) = player_transform.get_single() else {
        return;
    };
    let Ok(projection) = camera.get_single() else {
        return;
    };
    let center = transform.translation;
    let bounds =
        Rect::from_center_size(center.truncate(), projection.area.size() / SUBPIXELS as f32);
    commands.insert_resource(ScreenBounds(bounds));
}

pub(super) fn camera_plugin(app: &mut App) {
    app.fn_plugin(self::postprocess::postprocess_plugin)
        .add_systems(Startup, spawn_camera)
        .add_systems(
            PostUpdate,
            (
                track_player
                    .run_if(in_state(GameState::Running))
                    // sequence it here so we get the up-to-date physics-ed transform
                    .after(PhysicsSet::Writeback)
                    .before(TransformSystem::TransformPropagate),
                trails::spawn_ghost_trails
                    .run_if(not(resource_exists::<Paused>))
                    .after(TransformSystem::TransformPropagate),
            ),
        )
        .add_systems(
            Update,
            (
                rescale_outer_camera.run_if(any_with_component::<OuterCamera>),
                set_flip_x.after(GameTickSet::SetVelocity),
                update_screen_bounds,
            ),
        )
        .register_type::<ZLayer>();
}
