use bevy::prelude::*;
use bevy_rapier2d::prelude::Velocity;

/// If present, then this entity's [`Sprite`] and/or [`TextureAtlasSprite`] will
/// have their `flip_x` set depending on the sign of the entity's x-velocity. If
/// the entity does not have a `Sprite/TextureAtlasSprite` and a `Velocity`,
/// the system will not apply.
///
/// This assumes that your sprites are facing right (+x) by default.
#[derive(Debug, Component)]
pub struct AutoFlip;

#[allow(clippy::type_complexity)]
pub(super) fn set_flip_x(mut query: Query<(&mut Sprite, &Velocity), With<AutoFlip>>) {
    for (mut sprite, velocity) in &mut query {
        if velocity.linvel.x == 0.0 {
            continue;
        }
        sprite.flip_x = velocity.linvel.x < 0.0;
    }
}

#[cfg(test)]
mod testing {
    use super::*;
    use crate::testing::CommonTestPlugins;

    #[test]
    fn doesnt_change_flip_with_zero_velocity() {
        let mut app = App::new();
        app.add_plugins(CommonTestPlugins)
            .add_systems(Update, set_flip_x);
        let non_flipped = app
            .world
            .spawn((AutoFlip, Sprite::default(), Velocity::linear(Vec2::ZERO)))
            .id();
        let flipped = app
            .world
            .spawn((
                AutoFlip,
                Sprite {
                    flip_x: true,
                    ..default()
                },
                Velocity::linear(Vec2::ZERO),
            ))
            .id();
        app.update();
        assert!(
            !app.world.get::<Sprite>(non_flipped).unwrap().flip_x,
            "non-flipped entity should stay non-flipped with zero x-velocity"
        );
        assert!(
            app.world.get::<Sprite>(flipped).unwrap().flip_x,
            "flipped entity should stay flipped with zero x-velocity"
        );
    }
}
