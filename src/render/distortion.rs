//! Structures for indicating where to put screenspace distortions.
use bevy::prelude::*;
use bevy_tweening::Lens;

use super::{
    postprocess::{DistortionStorage, PostprocessMaterial},
    PlayerCamera, VirtualScreen,
};

/// Indicates that there should be a circular distortion at this position.
/// Anything with this component should have a transform; you can use
/// [`DistortionBundle`] to ensure that. The radius is in worldspace units.
#[derive(Component, Default, Reflect)]
pub struct Distortion {
    pub radius: f32,
    /// Controls various factors of the distortion. In the range [0.0, 1.0].
    pub strength: f32,
}

/// A distortion together with its location. The transform is in worldspace
/// units.
#[derive(Bundle, Default)]
pub struct DistortionBundle {
    pub distortion: Distortion,
    pub transform: TransformBundle,
}

/// Tweens a distortion's strength.
pub struct DistortionStrengthLens {
    pub start: f32,
    pub end: f32,
}

/// Tweens a distortion's radius.
pub struct DistortionRadiusLens {
    pub start: f32,
    pub end: f32,
}

impl Lens<Distortion> for DistortionStrengthLens {
    fn lerp(&mut self, target: &mut Distortion, ratio: f32) {
        target.strength = self.start + (self.end - self.start) * ratio;
    }
}

impl Lens<Distortion> for DistortionRadiusLens {
    fn lerp(&mut self, target: &mut Distortion, ratio: f32) {
        target.radius = self.start + (self.end - self.start) * ratio;
    }
}

pub(super) fn sync_distortions(
    mut materials: ResMut<Assets<PostprocessMaterial>>,
    screens: Query<&Handle<PostprocessMaterial>, With<VirtualScreen>>,
    distortions: Query<(&Distortion, &GlobalTransform)>,
    player_camera: Query<(&GlobalTransform, &Camera), With<PlayerCamera>>,
) {
    let (player_camera_transform, player_camera) = player_camera.single();
    let distortions: Vec<DistortionStorage> = distortions
        .iter()
        .flat_map(|(dis, transform)| {
            // coordinates in the virtual screen's space; i.e., (0, 0) is in the top-left of
            // the virtual screen and the bottom right is (VIRTUAL_SCREEN_SIZE,
            // VIRTUAL_SCREEN_SIZE).
            let mut ndc =
                player_camera.world_to_ndc(player_camera_transform, transform.translation())?;
            // NDCs have +y pointing down
            ndc.y *= -1.0;
            let uvs = (ndc.truncate() + 1.0) / 2.0;
            // but the virtual screen isn't the
            Some(DistortionStorage {
                center: uvs,
                radius: dis.radius / super::INNER_WIDTH as f32,
                strength: dis.strength,
            })
        })
        .collect();
    // this will waste effort writing to a material more than once if it's
    // duplicated, but that probably won't happen anyway, so.
    for handle in screens.iter() {
        if let Some(material) = materials.get_mut(handle) {
            material.update_distortions(distortions.clone());
        }
    }
}
