//! Defines a material for applying postprocessing shaders. This is used in
//! concert with render targets for cool shader effects.
use bevy::prelude::*;
use bevy::render::render_resource::{AsBindGroup, ShaderRef, ShaderType};
use bevy::sprite::{Material2d, Material2dPlugin};
use bevy::transform::TransformSystem;
use bevy_tweening::component_animator_system;

use super::distortion::{sync_distortions, Distortion};

#[derive(Clone, Default, Debug, Reflect, ShaderType)]
pub(super) struct DistortionStorage {
    pub center: Vec2,
    pub radius: f32,
    /// A strength of 0 skips processing entirely.
    pub strength: f32,
}

/// Material applied to the game world to do fancy shader effects. Currently,
/// only does 'postprocesss' for bombs.
///
/// You can use the default impl and just pass in the image handle.
///
/// Note that we assume that there's only one instance of this.
#[derive(AsBindGroup, Reflect, Clone, Default, Asset)]
pub(super) struct PostprocessMaterial {
    /// A pair of random floats used for spatially-varying randomness.
    #[uniform(0)]
    seed: Vec2,
    /// Time, used for temporal randomness and flickering the postprocess
    /// spheres.
    #[uniform(0)]
    time: f32,
    /// Locations of the distortions. We use an array because the web build
    /// can't handle storage buffers.
    #[uniform(1)]
    distortions: [DistortionStorage; 8], // keep in sync with wgsl
    #[texture(2)]
    #[sampler(3)]
    pub source_image: Handle<Image>,
}

impl PostprocessMaterial {
    pub(super) fn new(source_image: Handle<Image>) -> Self {
        Self {
            seed: default(),
            time: 0.0,
            distortions: default(),
            source_image,
        }
    }

    /// Updates the list of distortions from a vec of (center, radius) tuples.
    pub(super) fn update_distortions(&mut self, mut distortions: Vec<DistortionStorage>) {
        distortions.resize_with(8, default);
        self.distortions = distortions.try_into().unwrap();
    }
}

impl Material2d for PostprocessMaterial {
    fn fragment_shader() -> ShaderRef {
        "shaders/postprocess.wgsl".into()
    }
}

/// Updates the seed and the time in the postprocessing material.
fn update_postprocess_material(
    mut materials: ResMut<Assets<PostprocessMaterial>>,
    time: Res<Time>,
) {
    for (_, material) in materials.iter_mut() {
        material.seed = Vec2::new(fastrand::f32(), fastrand::f32());
        material.time = time.elapsed().as_secs_f32();
    }
}

pub(super) fn postprocess_plugin(app: &mut App) {
    app.add_plugins(Material2dPlugin::<PostprocessMaterial>::default())
        .register_type::<Distortion>()
        .add_systems(
            Update,
            (
                update_postprocess_material,
                component_animator_system::<Distortion>,
            ),
        )
        .add_systems(
            PostUpdate,
            sync_distortions.after(TransformSystem::TransformPropagate),
        );
}
