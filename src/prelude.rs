//! A module that imports preludes used throughout tengoku. Don't put stuff here
//! unless you're fine with this being imported *everywhere*.
pub use bevy::prelude::*;
pub use bevy_asset_loader::prelude::*;
pub use bevy_egui::{egui, EguiContexts};
pub use bevy_rapier2d::prelude::*;
pub use itertools::Itertools;
pub use rand::prelude::*;
pub use seldom_fn_plugin::FnPluginExt;
