//! Code related to how the game interacts with rapier. This doesn't implement
//! physics itself.

use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::scheduling::DespawnDuringCleanup;

// Defines collision groups. Notes:
// - Filtering is the responsibility for the thing that 'does' the interacting.
//   Hurtboxes should collide with everything and rely on hitboxes to filter out
//   the things they don't collide with.
// - In general, things with a {player,enemy} hitbox should include
//   {enemy,player} hurtboxes in their filters.

pub const GROUP_PLAYER_HITBOX: Group = Group::GROUP_1;
pub const GROUP_PLAYER_HURTBOX: Group = Group::GROUP_2;
pub const GROUP_ENEMY_HITBOX: Group = Group::GROUP_3;
pub const GROUP_ENEMY_HURTBOX: Group = Group::GROUP_4;
/// Marks valid spawn locations.
pub const GROUP_ALLOW_SPAWN: Group = Group::GROUP_5;
/// Don't spawn things if they would collide with this. Note that we only check
/// the *center* for now.
pub const GROUP_BLOCK_SPAWN: Group = Group::GROUP_6;
pub const GROUP_SPAWN_CHECK: Group = Group::GROUP_7;

/// Anything with this component will be marked for destruction when it collides
/// with something. The actual destruction is delayed so that things that happen
/// after physics writeback can still see the entity.
#[derive(Component)]
pub struct DestroyOnCollision;

fn destroy_on_collision(
    mut commands: Commands,
    query: Query<(Entity, &CollidingEntities), With<DestroyOnCollision>>,
) {
    for (entity, colliding) in &query {
        if !colliding.is_empty() {
            commands.entity(entity).insert(DespawnDuringCleanup);
        }
    }
}

pub fn common_physics_plugin(app: &mut App) {
    app.add_systems(
        PostUpdate,
        destroy_on_collision.before(PhysicsSet::Writeback),
    );
}
