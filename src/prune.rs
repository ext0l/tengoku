use std::time::Duration;

use bevy::prelude::*;

use crate::{
    render::ScreenBounds,
    scheduling::{DespawnDuringCleanup, GameState},
};

/// An entity with this component despawns if it's offscreen for more than the
/// given timer. Being onscreen, even for a single tick, resets the timer.
#[derive(Debug, Component)]
pub struct PruneIfOffscreen {
    timer: Timer,
}

impl PruneIfOffscreen {
    pub fn new(duration: Duration) -> Self {
        Self {
            timer: Timer::new(duration, TimerMode::Once),
        }
    }
}

fn prune_offscreen(
    mut commands: Commands,
    mut query: Query<(Entity, &mut PruneIfOffscreen, &GlobalTransform)>,
    screen_bounds: Res<ScreenBounds>,
    time: Res<Time>,
) {
    for (entity, mut prune, transform) in &mut query {
        let position = transform.translation().truncate();
        if screen_bounds.0.contains(position) {
            prune.timer.reset();
        } else {
            prune.timer.tick(time.delta());
            if prune.timer.finished() {
                commands.entity(entity).insert(DespawnDuringCleanup);
            }
        }
    }
}

pub fn prune_plugin(app: &mut App) {
    app.add_systems(
        Update,
        prune_offscreen
            .run_if(in_state(GameState::Running).and_then(resource_exists::<ScreenBounds>)),
    );
}
