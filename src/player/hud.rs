use bevy::prelude::*;

use crate::{
    actor::Enemy,
    damage::ZeroHPEvent,
    scheduling::{GameState, UiSet},
    ui::fonts::FontAssets,
};

/// Marker for the HUD node with the kill counter.
#[derive(Component)]
struct KillCounter {
    pub amount: u32,
    pub style: TextStyle,
}

fn setup_ui(mut commands: Commands, assets: Res<FontAssets>) {
    // Spawn text at the given position with the given color. We wrap this in a
    // closure since we spawn two copies of the killcounter for a drop shadow
    // effect.
    let mut spawn_text = move |position: UiRect, color: Color| {
        commands.spawn((
            TextBundle {
                style: Style {
                    position_type: PositionType::Absolute,
                    margin: UiRect::all(Val::Px(20.0)),
                    left: position.left,
                    top: position.top,
                    bottom: position.bottom,
                    right: position.right,
                    ..default()
                },
                ..default()
            },
            KillCounter {
                amount: 0,
                style: TextStyle {
                    font_size: 64.0,
                    color,
                    font: assets.silver.clone(),
                },
            },
        ));
    };
    spawn_text(
        UiRect::new(default(), Val::Px(0.0), Val::Px(4.0), default()),
        Color::BLACK,
    );
    spawn_text(
        UiRect::new(default(), Val::Px(4.0), Val::Px(0.0), default()),
        Color::WHITE,
    );
}

fn update_kill_counter(
    mut query: Query<&mut KillCounter>,
    mut events: EventReader<ZeroHPEvent>,
    enemies: Query<&Enemy>,
) {
    let deaths = events.read().filter(|ev| enemies.contains(ev.0)).count() as u32;
    for mut counter in &mut query {
        counter.amount += deaths;
    }
}

fn redraw_kill_counter(mut query: Query<(&mut Text, &KillCounter)>) {
    for (mut text, counter) in &mut query {
        *text = Text::from_section(format!("Kills: {}", counter.amount), counter.style.clone());
    }
}

pub fn hud_plugin(app: &mut App) {
    app.add_systems(
        Update,
        (update_kill_counter, redraw_kill_counter)
            .in_set(UiSet)
            .chain(),
    )
    .add_systems(OnEnter(GameState::Running), setup_ui);
}
