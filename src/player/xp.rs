//! System for managing player XP.
//!
//! The player gains XP in two ways: directly killing enemies and  picking up XP
//! orbs. Enemies always give some amount of XP, but have a chance to drop an
//! orb worth more XP.

use crate::physics::GROUP_PLAYER_HURTBOX;
use crate::prelude::*;
use crate::render::animation::{AnimationMode, AnimationState, Animations};
use crate::render::ZLayer;
use crate::scheduling::DespawnDuringCleanup;
use crate::{
    actor::{Enemy, Player},
    damage::ZeroHPEvent,
    scheduling::{GameState, GameTickSet},
};

/// Sent whenever the player levels up.
#[derive(Debug, Event)]
pub struct LevelUpEvent;

/// Represents the player's experience points. The player starts at level 0,
/// since this is a game about computers. :)
#[derive(Component, Copy, Clone, Default, Debug)]
pub struct Xp {
    level: u32,
    amount: u32,
}

impl Xp {
    /// Adds some amount of XP.
    pub fn add(&mut self, amount: u32) {
        self.amount += amount;
        while self.amount >= self.total_to_next_level() {
            self.amount -= self.total_to_next_level();
            self.level += 1;
        }
    }

    pub fn amount(&self) -> u32 {
        self.amount
    }

    /// Total amount of XP necessary to get to the next level (not including
    /// this partial level).
    pub fn total_to_next_level(&self) -> u32 {
        (self.level + 1) * 16
    }

    pub fn ratio(&self) -> f32 {
        (self.amount as f32) / (self.total_to_next_level() as f32)
    }
}

/// An entity with this component has a chance to drop an XP orb worth the given
/// amount.
#[derive(Component, Copy, Clone, Debug)]
pub struct DropsXp {
    pub chance: f32,
    pub amount: u32,
}

/// Indicates the XP to be awarded (when killed if this is an enemy; when picked
/// up if this is an XP orb).
#[derive(Component, Copy, Clone, Debug, Deref)]
pub struct XpValue(pub u32);

/// Marker component for being an XP orb.
#[derive(Component, Copy, Clone, Debug)]
struct XpOrb;

fn spawn_xp_orbs(
    mut commands: Commands,
    mut events: EventReader<ZeroHPEvent>,
    enemies: Query<(&GlobalTransform, &DropsXp)>,
    assets: Res<XpAssets>,
) {
    for (transform, drops_xp) in events.read().flat_map(|ev| enemies.get(ev.0)) {
        if fastrand::f32() < drops_xp.chance {
            let pos = transform.compute_transform().translation.round();
            commands.spawn((
                SpriteSheetBundle {
                    atlas: TextureAtlas {
                        layout: assets.layout.clone(),
                        index: 0,
                    },
                    texture: assets.orb.clone(),
                    transform: Transform::from_translation(pos),
                    ..default()
                },
                ZLayer::Fx,
                AnimationState::new(
                    "on_ground",
                    AnimationMode::RepeatForever,
                    assets.animation.clone(),
                ),
                XpValue(drops_xp.amount),
                XpOrb,
                Collider::ball(2.5),
                CollidingEntities::default(),
                ActiveEvents::COLLISION_EVENTS,
                Sensor,
                CollisionGroups::new(Group::all(), GROUP_PLAYER_HURTBOX),
            ));
        }
    }
}

fn award_xp_for_kills(
    mut events: EventReader<ZeroHPEvent>,
    mut player: Query<&mut Xp, With<Player>>,
    xp_value: Query<&XpValue, With<Enemy>>,
    mut level_up_events: EventWriter<LevelUpEvent>,
) {
    for event in events.read() {
        if let Ok(xp) = xp_value.get(event.0) {
            for mut player_xp in &mut player {
                let old_level = player_xp.level;
                player_xp.add(xp.0);
                if player_xp.level != old_level {
                    level_up_events.send(LevelUpEvent);
                }
            }
        }
    }
}

fn absorb_xp_orbs(
    mut commands: Commands,
    orbs: Query<(Entity, &CollidingEntities, &XpValue), With<XpOrb>>,
    mut xp: Query<&mut Xp>,
    mut level_up_events: EventWriter<LevelUpEvent>,
) {
    for (orb, colliding, XpValue(xp_value)) in &orbs {
        for collider in colliding.iter() {
            if let Ok(mut xp) = xp.get_mut(collider) {
                let old_level = xp.level;
                xp.add(*xp_value);
                if xp.level != old_level {
                    if xp.level > old_level + 1 {
                        error!("Somehow killed enough enemies to level up multiple times at once?");
                    }
                    level_up_events.send(LevelUpEvent);
                }
            }
            commands.entity(orb).insert(DespawnDuringCleanup);
        }
    }
}

#[derive(AssetCollection, Resource)]
struct XpAssets {
    #[asset(texture_atlas(tile_size_x = 8., tile_size_y = 8., columns = 4, rows = 1))]
    layout: Handle<TextureAtlasLayout>,
    #[asset(path = "objects/xp orb.png")]
    orb: Handle<Image>,
    #[asset(path = "objects/xp orb.animation.toml")]
    animation: Handle<Animations>,
}

pub(super) fn xp_plugin(app: &mut App) {
    app.configure_loading_state(
        LoadingStateConfig::new(GameState::Loading).load_collection::<XpAssets>(),
    )
    .add_systems(
        PostUpdate,
        (
            award_xp_for_kills,
            spawn_xp_orbs.run_if(in_state(GameState::Running)),
        )
            .after(GameTickSet::ApplyDamage),
    )
    .add_systems(PostUpdate, absorb_xp_orbs.after(PhysicsSet::Writeback))
    .add_event::<LevelUpEvent>();
}
