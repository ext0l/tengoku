use bevy::{
    prelude::*,
    render::{Extract, RenderApp},
    sprite::{extract_sprites, ExtractedSprite, ExtractedSprites, SpriteSystem},
};

use crate::{damage::HP, scheduling::UiSet};

/// Component that indicates that this thing is a health bar.
#[derive(Component, Debug)]
pub struct HealthBar {
    /// The entity that this is tracking the health of. We do this instead of
    /// assuming the parent is the relevant entity in case we need this to stop
    /// being a child for whatever reason.
    pub target: Entity,
    /// The target's HP. This is automatically updated by the health bar
    /// systems. If the target doesn't have any HP, this won't render.
    hp: Option<HP>,
}

impl HealthBar {
    pub fn new(target: Entity) -> Self {
        Self { target, hp: None }
    }
}

fn update_health_bars(mut health_bars: Query<&mut HealthBar>, hp_query: Query<Option<&HP>>) {
    for mut health_bar in &mut health_bars {
        let hp = hp_query
            .get(health_bar.target)
            .expect("getting an Option<&HP> should always succeed");
        health_bar.hp = hp.cloned();
    }
}

fn extract_health_bars(
    mut commands: Commands,
    mut extracted_sprites: ResMut<ExtractedSprites>,
    health_bars: Extract<Query<(Entity, &GlobalTransform, &HealthBar, &ViewVisibility)>>,
) {
    for (original_entity, transform, health_bar, visibility) in &health_bars {
        if !visibility.get() {
            continue;
        }
        let Some(HP(hp)) = health_bar.hp else {
            continue;
        };
        let hp_ratio = (hp as f32 / 10.0).max(0.0);
        let entity = commands.spawn_empty().id();
        extracted_sprites.sprites.insert(
            entity,
            ExtractedSprite {
                original_entity: Some(original_entity),
                transform: *transform,
                color: Color::GREEN,
                rect: None,
                custom_size: Some(Vec2::new(hp_ratio * 12.0, 2.0)),
                image_handle_id: default(),
                flip_x: false,
                flip_y: false,
                anchor: default(),
            },
        );
    }
}

pub fn health_bar_plugin(app: &mut App) {
    app.add_systems(Update, update_health_bars.in_set(UiSet));
    app.get_sub_app_mut(RenderApp)
        .expect("health bar plugin requires a RenderApp")
        .add_systems(
            ExtractSchedule,
            extract_health_bars
                .after(extract_sprites)
                .in_set(SpriteSystem::ExtractSprites),
        );
}
