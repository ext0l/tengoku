mod health_bar;
mod hud;
pub mod input;
pub mod xp;

use std::time::Duration;

use leafwing_input_manager::prelude::*;

use self::input::Dashing;
use crate::{
    actor::{ActorParams, Player, PlayerParams},
    damage::{DamageMethod, DamageOnHit, HasIframes},
    dps_tracker::DpsTracker,
    physics::{GROUP_ENEMY_HURTBOX, GROUP_PLAYER_HITBOX, GROUP_PLAYER_HURTBOX},
    player::{health_bar::HealthBar, input::Action, xp::Xp},
    prelude::*,
    render::{
        animation::{AnimationMode, AnimationState, Animations},
        AutoFlip, ZLayer,
    },
    scheduling::{GameState, GameTickSet},
    tiled_tilemap::PlayerSpawnPoint,
    weapons::{AddWeapon, Aim, AvailableWeapons, WeaponType},
};

#[derive(AssetCollection, Resource)]
pub struct PlayerAssets {
    #[asset(texture_atlas(tile_size_x = 16., tile_size_y = 16., columns = 5, rows = 5))]
    layout: Handle<TextureAtlasLayout>,
    #[asset(path = "player/player.png")]
    texture: Handle<Image>,
    #[asset(path = "player/player.animation.toml")]
    animation: Handle<Animations>,
    #[asset(path = "ui/cursor.png")]
    aim_cursor: Handle<Image>,
}

/// Marker that indicates that this component should be offset from its parent
/// corresponding to the parent's [`Aim`] component. This component will *not*
/// be rotated.
#[derive(Debug, Component)]
struct AimCursor {
    pub distance: f32,
}

/// Renders the cursor used to indicate where the player is aiming.
fn move_aim_cursor(
    aim_query: Query<&Aim>,
    mut cursor_query: Query<(&Parent, &AimCursor, &mut Transform)>,
) {
    for (parent, cursor, mut transform) in &mut cursor_query {
        if let Ok(aim) = aim_query.get(**parent) {
            transform.translation = (aim.0 * cursor.distance).extend(0.0);
        } else {
            warn!("Found an entity with AimCursor whose parent doesn't have Aim?");
        }
    }
}

/// Spawns the player by picking a random entity with a [`PlayerSpawnPoint`]
/// component and putting them there. Panics
fn spawn_player(
    mut commands: Commands,
    spawns: Query<&GlobalTransform, With<PlayerSpawnPoint>>,
    player_assets: Res<PlayerAssets>,
) {
    let spawns: Vec<_> = spawns.iter().collect();
    assert!(
        !spawns.is_empty(),
        "can't spawn the player without at least one PlayerSpawnPoint"
    );
    let mut spawn = spawns[fastrand::usize(0..spawns.len())].compute_transform();
    // necessary because spawn objects inherit the map's z-coordinate
    spawn.translation.z = 0.;
    commands
        .spawn((
            PlayerParams {
                actor: ActorParams {
                    collider: Collider::ball(6.),
                },
                hp: 10,
            }
            .into_bundle(),
            Xp::default(),
            AutoFlip,
            ZLayer::Player,
            DpsTracker::default(),
            Friction::new(0.0),
            HasIframes(Duration::from_millis(500)),
            ActionState::<Action>::default(),
            Action::input_map(),
            Aim(Vec2::X),
            Name::new("Player"),
            AvailableWeapons::player_weapons(),
        ))
        .insert(SpriteSheetBundle {
            atlas: TextureAtlas {
                layout: player_assets.layout.clone(),
                index: 0,
            },
            texture: player_assets.texture.clone(),
            transform: spawn,
            ..default()
        })
        .insert(AnimationState::new(
            "idle",
            AnimationMode::RepeatForever,
            player_assets.animation.clone(),
        ))
        // player should never be knocked back by collisions
        .insert(Dominance::group(127))
        .insert(CollisionGroups::new(GROUP_PLAYER_HURTBOX, Group::ALL))
        .with_children(|parent| {
            parent.spawn((
                HealthBar::new(parent.parent_entity()),
                SpatialBundle::from_transform(Transform::from_translation(Vec3::new(
                    0.0, 10.0, 0.0,
                ))),
                ZLayer::Ui,
            ));
            parent.spawn((
                AimCursor { distance: 40.0 },
                SpriteBundle {
                    texture: player_assets.aim_cursor.clone(),
                    ..default()
                },
                ZLayer::Ui,
            ));
        })
        .add(AddWeapon(WeaponType::Shotgun));
}

/// Marker component added to point to the hitbox the player gets when dashing.
#[derive(Debug, Component)]
struct DashingHitbox;

#[allow(clippy::type_complexity)]
fn on_start_dash(
    mut commands: Commands,
    started_dashing: Query<(Entity, &Collider), (With<Player>, Added<Dashing>)>,
) {
    for (entity, collider) in &started_dashing {
        commands.entity(entity).with_children(|parent| {
            parent.spawn((
                DashingHitbox,
                collider.clone(),
                CollidingEntities::default(),
                ActiveEvents::COLLISION_EVENTS,
                DamageOnHit {
                    amount: 20,
                    method: DamageMethod::Dash,
                },
                CollisionGroups::new(GROUP_PLAYER_HITBOX, GROUP_ENEMY_HURTBOX),
                Sensor,
                SpatialBundle::default(),
            ));
        });
    }
}

fn on_stop_dash(
    mut commands: Commands,
    children: Query<&Children>,
    hitbox: Query<&DashingHitbox>,
    mut stopped_dashing: RemovedComponents<Dashing>,
) {
    for has_hitbox in stopped_dashing
        .read()
        .filter_map(|entity| children.get(entity).ok())
        .flatten()
        .filter(|child| hitbox.contains(**child))
    {
        commands.entity(*has_hitbox).despawn_recursive();
    }
}

pub fn player_plugin(app: &mut App) {
    app.add_systems(OnEnter(GameState::Running), spawn_player)
        .add_systems(
            Update,
            (move_aim_cursor, on_start_dash, on_stop_dash).after(GameTickSet::PlayerInput),
        )
        .configure_loading_state(
            LoadingStateConfig::new(GameState::Loading).load_collection::<PlayerAssets>(),
        )
        .fn_plugin(health_bar::health_bar_plugin)
        .fn_plugin(hud::hud_plugin)
        .fn_plugin(xp::xp_plugin)
        .fn_plugin(input::input_plugin);
}
