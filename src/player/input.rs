//! Defines player input.
use std::time::Duration;

use leafwing_input_manager::prelude::*;

use super::Aim;
use crate::{
    prelude::*,
    render::animation::{AnimationMode, AnimationState},
    scheduling::{GameState, GameTickSet},
};

/// If set on the player, then the player's movement is fixed at its current
/// velocity. Automatically removes itself when the timer expires.
#[derive(Debug, Clone, Component, Deref, DerefMut)]
pub struct Dashing(pub Timer);

#[derive(Debug, Clone, Actionlike, Reflect, Hash, PartialEq, Eq)]
pub enum Action {
    /// Moves the player around the map.
    Move,
    /// Sets where the player is aiming.
    Aim,
    /// Dashes the player in the direction they're currently moving.
    Dash,
}

impl Action {
    /// Construct the input map used to bind inputs to actions.
    pub fn input_map() -> InputMap<Self> {
        InputMap::default()
            .insert(Action::Move, DualAxis::left_stick())
            .insert(Action::Move, VirtualDPad::wasd())
            .insert(Action::Aim, DualAxis::right_stick())
            .insert(Action::Aim, VirtualDPad::arrow_keys())
            .insert(Action::Dash, GamepadButtonType::RightTrigger2)
            .insert(Action::Dash, KeyCode::Space)
            .build()
    }
}

fn tick_dash_timer(
    mut commands: Commands,
    mut query: Query<(Entity, &mut Dashing)>,
    time: Res<Time>,
) {
    for (entity, mut dashing) in &mut query {
        dashing.tick(time.delta());
        if dashing.finished() {
            commands.entity(entity).remove::<Dashing>();
        }
    }
}

fn start_dashing(
    mut commands: Commands,
    mut query: Query<(Entity, &ActionState<Action>, &mut Velocity), Without<Dashing>>,
) {
    for (entity, actions, mut velocity) in &mut query {
        if actions.just_pressed(&Action::Dash) {
            let direction = velocity.linvel.try_normalize().unwrap_or(Vec2::X);
            velocity.linvel = direction * 800.0;
            commands.entity(entity).insert(Dashing(Timer::new(
                Duration::from_millis(300),
                TimerMode::Once,
            )));
        }
    }
}

fn handle_movement(
    mut query: Query<(&ActionState<Action>, &mut Velocity, &mut AnimationState), Without<Dashing>>,
) {
    for (action_state, mut velocity, mut animation) in &mut query {
        let movement = action_state
            .axis_pair(&Action::Move)
            .map_or(Vec2::ZERO, |axes| axes.xy())
            .normalize_or_zero();
        velocity.linvel = movement * 60.0;
        animation.play(
            if movement.length() > 0.0 {
                "walk"
            } else {
                "idle"
            },
            AnimationMode::RepeatForever,
        );
    }
}

fn handle_aim(mut query: Query<(&ActionState<Action>, &mut Aim)>) {
    for (action_state, mut aim) in &mut query {
        // aiming
        if let Some(aim_axes) = action_state.axis_pair(&Action::Aim) {
            aim.set(aim_axes.xy())
        }
    }
}

fn disable_input(mut commands: Commands) {
    commands.insert_resource(ToggleActions::<Action>::DISABLED);
}

fn enable_input(mut commands: Commands) {
    commands.insert_resource(ToggleActions::<Action>::ENABLED);
}

pub(super) fn input_plugin(app: &mut App) {
    app.add_plugins(InputManagerPlugin::<Action>::default())
        .add_systems(
            Update,
            // need to apply_deferred because tick_dash_timer can get rid of dashing and
            // start_dashing can add it
            (
                tick_dash_timer,
                apply_deferred,
                start_dashing,
                apply_deferred,
                handle_movement,
                handle_aim,
            )
                .chain()
                .in_set(GameTickSet::PlayerInput),
        )
        .add_systems(OnEnter(GameState::Running), enable_input)
        .add_systems(OnExit(GameState::Running), disable_input);
}
