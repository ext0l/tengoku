use std::time::Duration;

use bevy::prelude::*;

use crate::scheduling::GameTickSet;

/// Automatically recursively despawns entities after a certain amount of
/// time. To use this, attach a [`DespawnTimer`] to your entity.
pub struct DespawnTimerPlugin;

/// When the timer goes off, this entity will be despawned.
#[derive(Component)]
pub struct DespawnTimer(Timer);

impl DespawnTimer {
    pub fn new(duration: Duration) -> Self {
        Self(Timer::new(duration, TimerMode::Once))
    }
}

impl Plugin for DespawnTimerPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Last, reap.before(GameTickSet::Cleanup));
    }
}

/// Implementation for the despawn plugin. Ticks all despawn timers and removes
/// entities whose time is up.
fn reap(mut commands: Commands, mut query: Query<(Entity, &mut DespawnTimer)>, time: Res<Time>) {
    for (entity, mut timer) in query.iter_mut() {
        timer.0.tick(time.delta());
        if timer.0.finished() {
            commands.entity(entity).despawn_recursive();
        }
    }
}
