//! Defines system sets and states for our various systems to add themselves
//! into.

use std::time::Duration;

use bevy::{ecs::system::BoxedSystem, time::TimeSystem};

use crate::prelude::*;

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug, Default, States)]
pub enum GameState {
    /// Loading assets at the start of the game. We don't have any loading
    /// screens after the first.
    #[default]
    Loading,
    /// Displayed when the game first starts up.
    MainMenu,
    /// Spawning in the map.
    CreatingMap,
    /// Actively playing the game; not in a menu, etc.
    Running,
    /// Only transitioned to if there was an error while loading assets. This
    /// should cause a panic.
    #[allow(dead_code)]
    LoadingError,
    /// The player died. Press F to pay respects.
    GameOver,
}

#[derive(Clone, PartialEq, Eq, Debug, Resource)]
pub struct Paused;

/// Set for things that have to do with UI. Purely organizational.
#[derive(SystemSet, PartialEq, Eq, Hash, Debug, Clone)]
pub struct UiSet;

#[derive(Clone, PartialEq, Eq, Debug, Hash, SystemSet)]
pub enum MainMenuSet {
    /// System set for things that should run when in the main menu. Use
    /// [`Update`].
    MainMenu,
}

/// Phases that are active when the game is currently active (the player is
/// moving and so on).
#[derive(Clone, PartialEq, Eq, Debug, Hash, SystemSet)]
pub enum GameTickSet {
    /// Handles the player's input.
    PlayerInput,
    /// Systems that set entities' velocity.
    SetVelocity,
    /// Things that generate damage events.
    GenerateDamage,
    /// Applies damage to things that need to be damaged.
    ApplyDamage,
    /// Destroys all entities that are meant to die on this update, but whose
    /// death was delayed. For example, projectiles are marked for cleanup after
    /// physics writeback, but they shouldn't *actually* be destroyed until
    /// later because damage processing hasn't happened yet.
    Cleanup,
}

/// Allows plugins to register that they want the game to be paused under
/// certain conditions.
#[derive(Debug, Default, Resource)]
pub struct PauseConditions(Vec<(String, BoxedSystem<(), bool>)>);

impl PauseConditions {
    /// If any of the systems return true, inserts the `Paused` resource.
    /// Otherwise, removes it.
    fn pause_or_unpause(world: &mut World) {
        let should_be_paused = world.resource_scope(|world, mut locks: Mut<Self>| {
            locks.0.iter_mut().any(|(name, lock)| {
                if lock.run((), world) {
                    debug!("pausing because of lock: {}", name);
                    true
                } else {
                    false
                }
            })
        });
        if should_be_paused {
            world.insert_resource(Paused);
        } else {
            world.remove_resource::<Paused>();
        }
    }
}

pub trait PauseConditionExt {
    /// Indicates that the game should be paused if the given system returns
    /// true.
    fn pause_if<Marker>(
        &mut self,
        name: impl AsRef<str>,
        system: impl IntoSystem<(), bool, Marker>,
    ) -> &mut Self;
}

impl PauseConditionExt for App {
    fn pause_if<Marker>(
        &mut self,
        name: impl AsRef<str>,
        system: impl IntoSystem<(), bool, Marker>,
    ) -> &mut Self {
        let world: &mut World = &mut self.world;
        let mut system = IntoSystem::into_system(system);
        world.resource_scope(|world, mut locks: Mut<PauseConditions>| {
            system.initialize(world);
            locks.0.push((name.as_ref().to_owned(), Box::new(system)));
        });
        self
    }
}

/// Total amount of time that the game has been running (i.e., the state is in
/// `GameState::InGame` and `InGameMenu::Running`). This resets when the game
/// starts. This is meant for things like enemy scaling that go up over time.
#[derive(Clone, PartialEq, Eq, Debug, Default, Resource, Deref, Reflect)]
#[reflect(Resource)]
pub struct RunningTime(Duration);

impl RunningTime {
    pub fn advance(time: Res<Time>, mut running_time: ResMut<RunningTime>) {
        running_time.0 += time.delta();
    }
}

/// All entities with this component will be (recursively) despawned during the
/// cleanup set.
#[derive(Component)]
#[component(storage = "SparseSet")]
pub struct DespawnDuringCleanup;

fn scheduled_despawns(
    mut commands: Commands,
    mut query: Query<Entity, With<DespawnDuringCleanup>>,
) {
    for entity in &mut query {
        commands.entity(entity).despawn_recursive();
    }
}

fn pause_time(mut time: ResMut<Time<Virtual>>) {
    time.pause();
}

fn unpause_time(mut time: ResMut<Time<Virtual>>) {
    time.unpause();
}

/// Sets up scheduling-related systems.
pub fn common_scheduling_plugin(app: &mut App) {
    app.init_resource::<PauseConditions>()
        .configure_sets(
            Update,
            (
                GameTickSet::SetVelocity.before(PhysicsSet::SyncBackend),
                UiSet,
            ),
        )
        .configure_sets(Last, (GameTickSet::Cleanup,))
        .configure_sets(
            PostUpdate,
            (GameTickSet::GenerateDamage, GameTickSet::ApplyDamage)
                .chain()
                .after(PhysicsSet::Writeback),
        )
        .configure_sets(
            Update,
            MainMenuSet::MainMenu.run_if(in_state(GameState::MainMenu)),
        )
        .init_resource::<RunningTime>()
        // in non-Running states, time is paused, so we can run the system and it'll just do nothing
        .add_systems(
            First,
            RunningTime::advance
                .run_if(in_state(GameState::Running))
                .after(TimeSystem),
        )
        .add_systems(Last, scheduled_despawns.in_set(GameTickSet::Cleanup))
        .add_systems(
            PreUpdate,
            (
                PauseConditions::pause_or_unpause.run_if(in_state(GameState::Running)),
                apply_deferred,
                pause_time.run_if(resource_added::<Paused>),
                unpause_time.run_if(resource_removed::<Paused>()),
            )
                .chain(),
        )
        .register_type::<RunningTime>();
}

#[cfg(test)]
mod tests {
    use seldom_fn_plugin::FnPluginExt;

    use super::*;
    use crate::testing::CommonTestPlugins;

    #[test]
    fn test_despawn_during_cleanup() {
        let mut app = App::new();
        app.add_plugins(CommonTestPlugins)
            .fn_plugin(common_scheduling_plugin);
        let entity = app.world.spawn(DespawnDuringCleanup).id();
        app.update();
        assert!(!app.world.entities().contains(entity));
    }
}
