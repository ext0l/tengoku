use std::time::Duration;

use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::{
    player::input::Dashing,
    scheduling::GameTickSet,
    weapons::{WeaponStats, WeaponType},
};

/// Handles damaging collisions, 'direct' damage applications
/// (hitscan/poison/etc), and removing entities with zero hp.
pub struct DamagePlugin;

/// Tag to indicate how damage was dealt. This is mostly used for DPS graphs
/// (how much damage did your shotgun/missile/laser do) but it could also be
/// used for "what killed me" or similar.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Component)]
pub enum DamageMethod {
    /// A weapon (presumably fired by the player).
    Weapon { kind: WeaponType, level: u32 },
    /// Dash-damage.
    Dash,
    /// Killed by the architects.
    Debug,
    /// The damage type for enemies that hit the player. Will probably get a
    /// field containing what *type* of enemy when I have more than one enemy.
    Enemy,
}

/// Send this event to indicate that the given entity should receive damage. If
/// it doesn't have an [`HP`] component, this will later cause a panic.
#[derive(Debug, Event)]
pub struct IncomingDamageEvent {
    pub source: Entity,
    pub method: DamageMethod,
    pub target: Entity,
    pub amount: i32,
}

/// Sent by the damage systems whenever an entity *actually* loses HP.
#[derive(Debug, Event)]
pub struct ActualDamageEvent {
    pub source: Entity,
    pub method: DamageMethod,
    pub target: Entity,
    effective: i32,
    overkill: i32,
}

impl ActualDamageEvent {
    /// Total damage dealt: `self.effective() + self.overkill()`.
    pub fn total(&self) -> i32 {
        self.effective + self.overkill
    }
}

/// If set, any damage dealt will be accounted to this entity.
#[derive(Component, Clone)]
pub struct DamageSource(pub Entity);

/// Attach this to something to make it damage a player/enemy that it hits. The
/// `method` field will be copied into the `IncomingDamageEvent` accordingly.
#[derive(Component, Clone, Debug)]
pub struct DamageOnHit {
    pub amount: i32,
    pub method: DamageMethod,
}

impl DamageOnHit {
    pub fn from_weapon_stats(weapon: WeaponType, stats: &WeaponStats) -> Self {
        Self {
            amount: stats.damage as i32,
            method: DamageMethod::Weapon {
                kind: weapon,
                level: stats.level,
            },
        }
    }
}

/// When hit, the entity will become invulnerable for the given duration.
#[derive(Debug, Component)]
pub struct HasIframes(pub Duration);

/// Active iframes for the given entity.
#[derive(Debug, Component, Deref, DerefMut)]
#[component(storage = "SparseSet")]
pub struct Iframes(pub Timer);

impl Iframes {
    pub fn new(duration: Duration) -> Self {
        Self(Timer::new(duration, TimerMode::Once))
    }
}

/// Invulnerability from a debugging system.
#[derive(Debug, Component)]
#[component(storage = "SparseSet")]
pub struct DebugInvulnerability;

// "Vulnerable" might get used for something else.
pub type CanTakeDamage<'w, 's> = (
    Without<Iframes>,
    Without<DebugInvulnerability>,
    Without<Dashing>,
);

/// Hit points. You know, like in video games!
///
/// This is i32 so that we can just subtract from it without having to do
/// saturated arithmetic everywhere.
#[derive(Clone, Debug, Component)]
pub struct HP(pub i32);

/// Fired whenever an entity goes from above zero HP to below zero HP. So this
/// will only fire once per entity.
#[derive(Clone, Debug, Component, Event)]
pub struct ZeroHPEvent(pub Entity);

impl Plugin for DamagePlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<IncomingDamageEvent>()
            .add_event::<ZeroHPEvent>()
            .add_event::<ActualDamageEvent>()
            .add_systems(
                PostUpdate,
                generate_damage_events.in_set(GameTickSet::GenerateDamage),
            )
            .add_systems(
                PostUpdate,
                (tick_iframes, apply_damage)
                    .chain()
                    .in_set(GameTickSet::ApplyDamage),
            );
    }
}

fn tick_iframes(mut commands: Commands, mut query: Query<(Entity, &mut Iframes)>, time: Res<Time>) {
    for (entity, mut iframes) in &mut query {
        iframes.tick(time.delta());
        if iframes.finished() {
            commands.entity(entity).remove::<Iframes>();
        }
    }
}

fn generate_damage_events(
    mut writer: EventWriter<IncomingDamageEvent>,
    has_hp: Query<Entity, With<HP>>,
    damage_on_hit: Query<(Entity, &DamageOnHit, &CollidingEntities)>,
) {
    for (source, on_hit, colliding_entities) in &damage_on_hit {
        for target in colliding_entities.iter().filter_map(|e| has_hp.get(e).ok()) {
            writer.send(IncomingDamageEvent {
                source,
                target,
                amount: on_hit.amount,
                method: on_hit.method.clone(),
            });
        }
    }
}

/// Process all damage events by applying them to their targets.
fn apply_damage(
    mut commands: Commands,
    mut reader: EventReader<IncomingDamageEvent>,
    mut query: Query<(&mut HP, Option<&HasIframes>), CanTakeDamage>,
    mut writer: EventWriter<ZeroHPEvent>,
    damage_source: Query<&DamageSource>,
    mut actual_damage_writer: EventWriter<ActualDamageEvent>,
) {
    for &IncomingDamageEvent {
        source,
        ref method,
        target,
        amount,
        ..
    } in reader.read()
    {
        let Ok((mut hp, has_iframes)) = query.get_mut(target) else {
            continue;
        };
        let old_hp = hp.0;
        hp.0 -= amount;

        if let Some(HasIframes(duration)) = has_iframes {
            commands.entity(target).insert(Iframes::new(*duration));
        }
        if old_hp > 0 && hp.0 <= 0 {
            writer.send(ZeroHPEvent(target));
        }

        let effective = amount.min(old_hp).max(0);
        let overkill = (amount - effective).max(0);
        let source = damage_source.get(source).map_or(source, |s| s.0);
        actual_damage_writer.send(ActualDamageEvent {
            source,
            method: method.clone(),
            target,
            effective,
            overkill,
        });
    }
}
