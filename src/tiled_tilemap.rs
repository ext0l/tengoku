// Copy-pasted(!) from the bevy_ecs_tilemap repository.

use std::{
    collections::HashMap,
    io::{self, Cursor},
    path::Path,
    sync::Arc,
};

use anyhow::Result;
use bevy::{
    asset::{io::Reader, AssetLoader, AssetPath},
    ecs::system::EntityCommands,
    log,
    prelude::*,
};
use bevy_asset_loader::prelude::*;
use bevy_ecs_tilemap::prelude::*;
use bevy_rapier2d::prelude::*;
use tiled::{FiniteTileLayer, LayerTile, ObjectData, ObjectShape, ResourceReader};

use crate::{
    physics::{GROUP_ALLOW_SPAWN, GROUP_SPAWN_CHECK},
    scheduling::GameState,
};

#[derive(Default)]
pub struct TiledMapPlugin;

impl Plugin for TiledMapPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.register_asset_loader(TiledLoader)
            .init_asset::<TiledMap>()
            .add_plugins(TilemapPlugin)
            .add_systems(
                Update,
                (
                    process_loaded_maps,
                    wait_for_map_to_spawn.run_if(in_state(GameState::CreatingMap)),
                ),
            )
            .configure_loading_state(
                LoadingStateConfig::new(GameState::Loading).load_collection::<MapAssets>(),
            );
    }
}

#[derive(AssetCollection, Resource)]
pub struct MapAssets {
    #[asset(path = "map/map.tmx")]
    pub map: Handle<TiledMap>,
}

#[derive(TypePath, Asset)]
pub struct TiledMap {
    pub map: tiled::Map,

    pub tilemap_textures: HashMap<usize, TilemapTexture>,
}

#[derive(Default, Bundle)]
pub struct TiledMapBundle {
    pub tiled_map: Handle<TiledMap>,
    pub spatial: SpatialBundle,
}

/// Indicates that this is where the player should spawn in the map.
#[derive(Component)]
pub struct PlayerSpawnPoint;

pub struct TiledLoader;

/// Indicates that this entity corresponds to a tmx layer. For tile layers, this
/// will have MapLayerWithTileset children.
#[derive(Component)]
pub struct MapLayer;

/// Indicates that this corresponds to some combination of layers and tilesets.
/// Since `bevy_ecs_tilemap` only supports one grid size per layer, we need one
/// of these for each (tmx tile layer, tileset) pair. The parent of this is a
/// [`MapLayer`].
#[derive(Component)]
pub struct MapLayerWithTileset;

impl AssetLoader for TiledLoader {
    type Asset = TiledMap;
    type Settings = ();
    type Error = anyhow::Error;

    fn load<'a>(
        &'a self,
        _reader: &'a mut Reader,
        _settings: &'a Self::Settings,
        load_context: &'a mut bevy::asset::LoadContext,
    ) -> bevy::asset::BoxedFuture<'a, Result<Self::Asset>> {
        Box::pin(async move {
            let tmx_path = load_context.path();

            let mut loader = tiled::Loader::with_cache_and_reader(
                tiled::DefaultResourceCache::new(),
                EmbeddedReader,
            );
            let map = loader
                .load_tmx_map(tmx_path)
                .map_err(|e| anyhow::anyhow!("Could not load TMX map: {e}"))?;

            let mut tilemap_textures = HashMap::default();

            for (tileset_index, tileset) in map.tilesets().iter().enumerate() {
                let tilemap_texture = match &tileset.image {
                    None => {
                        panic!("Image collection tileset {} not compatible", tileset.name)
                    }
                    Some(img) => {
                        let tile_path = img.source.to_owned();
                        info!("Loading image from {}", tile_path.display());
                        let asset_path = AssetPath::from(tile_path);
                        let texture: Handle<Image> = load_context.load(asset_path.clone());

                        TilemapTexture::Single(texture.clone())
                    }
                };

                tilemap_textures.insert(tileset_index, tilemap_texture);
            }

            let asset_map = TiledMap {
                map,
                tilemap_textures,
            };

            log::info!("Loaded map: {}", load_context.path().display());

            Ok(asset_map)
        })
    }

    fn extensions(&self) -> &[&str] {
        static EXTENSIONS: &[&str] = &["tmx"];
        EXTENSIONS
    }
}

const LAYER_SCALE: f32 = 1. / 128.;

pub fn process_loaded_maps(
    mut commands: Commands,
    maps: Res<Assets<TiledMap>>,
    mut new_maps: Query<(Entity, &Handle<TiledMap>), Added<Handle<TiledMap>>>,
) {
    if new_maps.is_empty() {
        return;
    }
    for (map_entity, map_handle) in &mut new_maps {
        let Some(tiled_map) = maps.get(map_handle) else {
            return;
        };

        for (layer_index, layer) in tiled_map.map.layers().enumerate() {
            let offset_x = layer.offset_x;
            let offset_y = layer.offset_y;

            let map_size = TilemapSize {
                x: tiled_map.map.width,
                y: tiled_map.map.height,
            };

            let grid_size = TilemapGridSize {
                x: tiled_map.map.tile_width as f32,
                y: tiled_map.map.tile_height as f32,
            };

            let map_type = match tiled_map.map.orientation {
                tiled::Orientation::Hexagonal => TilemapType::Hexagon(HexCoordSystem::Row),
                tiled::Orientation::Isometric => TilemapType::Isometric(IsoCoordSystem::Diamond),
                tiled::Orientation::Staggered => TilemapType::Isometric(IsoCoordSystem::Staggered),
                tiled::Orientation::Orthogonal => TilemapType::Square,
            };

            let layer_transform = get_tilemap_center_transform(
                &map_size,
                &grid_size,
                &map_type,
                layer_index as f32 / LAYER_SCALE,
            ) * Transform::from_xyz(offset_x, -offset_y, 0.0);

            let map_layer = commands
                .spawn((
                    MapLayer,
                    SpatialBundle::from_transform(layer_transform),
                    Name::new(layer.name.clone()),
                ))
                .set_parent(map_entity)
                .id();
            match layer.layer_type() {
                tiled::LayerType::Tiles(tiled::TileLayer::Finite(finite_layer)) => {
                    for (tileset_index, tileset) in tiled_map.map.tilesets().iter().enumerate() {
                        process_layer(
                            commands
                                .spawn((MapLayerWithTileset, SpatialBundle::default()))
                                .set_parent(map_layer),
                            tiled_map,
                            tileset_index,
                            tileset,
                            finite_layer,
                        );
                    }
                }
                tiled::LayerType::Objects(object_layer) => {
                    commands.entity(map_entity).with_children(|parent| {
                        parent
                            .spawn(TransformBundle::from_transform(layer_transform))
                            .with_children(|builder| process_object(builder, object_layer));
                    });
                }
                _ => log::warn!(
                    "Unsupported layer type for {} (id={})",
                    layer.name,
                    layer.id()
                ),
            }
        }
    }
}

/// Process a finite tile layer. The builder should be the `MapLayerWithTileset`
/// to spawn the tiles under.
fn process_layer(
    tileset_layer: &mut EntityCommands,
    tiled_map: &TiledMap,
    tileset_index: usize,
    tileset: &Arc<tiled::Tileset>,
    layer: FiniteTileLayer,
) {
    let grid_size = TilemapGridSize {
        x: tiled_map.map.tile_width as f32,
        y: tiled_map.map.tile_height as f32,
    };

    let map_type = match tiled_map.map.orientation {
        tiled::Orientation::Hexagonal => TilemapType::Hexagon(HexCoordSystem::Row),
        tiled::Orientation::Isometric => TilemapType::Isometric(IsoCoordSystem::Diamond),
        tiled::Orientation::Staggered => TilemapType::Isometric(IsoCoordSystem::Staggered),
        tiled::Orientation::Orthogonal => TilemapType::Square,
    };

    let Some(tilemap_texture) = tiled_map.tilemap_textures.get(&tileset_index) else {
        log::warn!("Skipped creating layer with missing tilemap textures.");
        return;
    };

    let tile_size = TilemapTileSize {
        x: tileset.tile_width as f32,
        y: tileset.tile_height as f32,
    };

    let tile_spacing = TilemapSpacing {
        x: tileset.spacing as f32,
        y: tileset.spacing as f32,
    };

    let map_size = TilemapSize {
        x: tiled_map.map.width,
        y: tiled_map.map.height,
    };

    let mut tile_storage = TileStorage::empty(map_size);
    for x in 0..map_size.x {
        for y in 0..map_size.y {
            let mut mapped_y = y;
            if tiled_map.map.orientation == tiled::Orientation::Orthogonal {
                mapped_y = (tiled_map.map.height - 1) - y;
            }

            let mapped_x = x as i32;
            let mapped_y = mapped_y as i32;

            let layer_tile = match layer.get_tile(mapped_x, mapped_y) {
                Some(t) => t,
                None => {
                    continue;
                }
            };
            if tileset_index != layer_tile.tileset_index() {
                continue;
            }
            let layer_tile_data = match layer.get_tile_data(mapped_x, mapped_y) {
                Some(d) => d,
                None => {
                    continue;
                }
            };

            let texture_index = match tilemap_texture {
                TilemapTexture::Single(_) => layer_tile.id(),
                _ => panic!("can't handle non-Single tilemap textures"),
            };

            let tile_pos = TilePos { x, y };
            let center = tile_pos.center_in_world(&grid_size, &map_type);
            tileset_layer.with_children(|layer_builder| {
                let mut tile_entity = layer_builder.spawn(TileBundle {
                    position: tile_pos,
                    tilemap_id: TilemapId(layer_builder.parent_entity()),
                    texture_index: TileTextureIndex(texture_index),
                    flip: TileFlip {
                        x: layer_tile_data.flip_h,
                        y: layer_tile_data.flip_v,
                        d: layer_tile_data.flip_d,
                    },
                    ..Default::default()
                });
                if let Some(collider) = tile_collider(layer_tile, tile_size) {
                    tile_entity.insert((
                        SpatialBundle {
                            visibility: Visibility::Inherited,
                            transform: Transform::from_translation(center.extend(0.)),
                            ..default()
                        },
                        RigidBody::Fixed,
                        Friction::new(0.0),
                        collider,
                    ));
                }
                tile_storage.set(&tile_pos, tile_entity.id());
            });
        }
    }

    tileset_layer.insert((
        TilemapBundle {
            grid_size,
            size: map_size,
            storage: tile_storage,
            texture: tilemap_texture.clone(),
            tile_size,
            spacing: tile_spacing,
            map_type,
            ..Default::default()
        },
        Name::new(tileset.name.clone()),
    ));
}

/// Construct the `Collider` for the given tile. If the tile is a wall, the
/// entire tile is a collider. Otherwise, each object defined on that tile is a
/// collider.
fn tile_collider(layer_tile: LayerTile, tile_size: TilemapTileSize) -> Option<Collider> {
    let tile = layer_tile.get_tile()?;
    let is_wall = tile
        .user_type
        .as_ref()
        .map_or(false, |type_| type_ == "wall");

    if is_wall {
        Some(Collider::cuboid(
            tile_size.x / 2.0 + 0.01,
            tile_size.y / 2.0 + 0.01,
        ))
    } else if let Some(collision) = &tile.collision {
        let center = Vec2::new(tile_size.x, tile_size.y) / 2.0;
        let colliders = collision
            .object_data()
            .iter()
            .map(|ObjectData { x, y, shape, .. }| match shape {
                ObjectShape::Rect { width, height } => (
                    // annoying vector math, since bevy positions are relative to the center but
                    // tiled positions are relative to the corner, *and* they use different y-axis
                    // conventions.
                    Vec2::new(*x + width / 2.0, *y - height / 2.0) - center,
                    0.0,
                    Collider::cuboid(width / 2.0, height / 2.0),
                ),
                _ => panic!("Don't know how to handle shape {shape:?}"),
            });
        Some(Collider::compound(colliders.collect()))
    } else {
        None
    }
}

fn process_object(builder: &mut ChildBuilder, layer: tiled::ObjectLayer) {
    let map_height = layer.map().height as f32 * layer.map().tile_height as f32;

    for object in layer.objects() {
        match object.user_type.as_str() {
            "player_spawn" => {
                info!("Registered spawn point at {} {}", object.x, -object.y);
                builder.spawn((
                    PlayerSpawnPoint,
                    TransformBundle::from_transform(Transform::from_xyz(
                        object.x,
                        map_height - object.y,
                        0.0,
                    )),
                ));
            }
            "enemy_spawn" => {
                info!("Registered enemy spawn");
                let ObjectShape::Rect { width, height } = object.shape else {
                    panic!("Unknown object shape {:?}", object.shape);
                };
                let collider = Collider::cuboid(width / 2., height / 2.);
                let translation = Vec3::new(
                    object.x + width / 2.,
                    map_height - object.y - height / 2.,
                    0.,
                );
                builder.spawn((
                    collider,
                    CollisionGroups::new(GROUP_ALLOW_SPAWN, GROUP_SPAWN_CHECK),
                    Sensor,
                    TransformBundle::from_transform(Transform::from_translation(translation)),
                ));
            }

            _ => warn!("Unknown object type {}", object.user_type),
        }
    }
}

fn wait_for_map_to_spawn(
    query: Query<Entity, With<Handle<TiledMap>>>,
    assets: Res<MapAssets>,
    mut commands: Commands,
    mut next_state: ResMut<NextState<GameState>>,
) {
    if !query.is_empty() {
        next_state.set(GameState::Running);
    } else {
        commands.spawn((
            TiledMapBundle {
                tiled_map: assets.map.clone(),
                ..default()
            },
            Name::new("Map"),
        ));
    }
}

/// A [`ResourceReader`] that compiles in all the map files. We use this instead
/// of runtime loading for wasm compatibility and because we're not actually
/// doing any dynamic reloading yet.
pub struct EmbeddedReader;

impl ResourceReader for EmbeddedReader {
    type Resource = Cursor<&'static [u8]>;

    type Error = io::Error;

    fn read_from(&mut self, path: &Path) -> std::result::Result<Self::Resource, Self::Error> {
        let bytes: &'static [u8] = if path == Path::new("map/map.tmx") {
            include_bytes!("../assets/map/map.tmx")
        } else if path == Path::new("map/tileset.tsx") {
            include_bytes!("../assets/map/tileset.tsx")
        } else if path == Path::new("map/obstacles-and-objects.tsx") {
            include_bytes!("../assets/map/obstacles-and-objects.tsx")
        } else {
            return Err(io::Error::new(
                io::ErrorKind::NotFound,
                format!("Unknown path {path:?}"),
            ));
        };
        Ok(Cursor::new(bytes))
    }
}
