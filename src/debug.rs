//! Development-mode stuff: inspector plugins and so on.

use bevy::{
    input::common_conditions::{input_just_pressed, input_toggle_active},
    prelude::*,
};
use bevy_console::{reply, AddConsoleCommand, ConsoleCommand, ConsoleOpen, ConsolePlugin};
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_rapier2d::render::{DebugRenderContext, RapierDebugRenderPlugin};
use clap::Parser;

use crate::{
    actor::{Enemy, Player},
    damage::{DamageMethod, DebugInvulnerability, IncomingDamageEvent, HP},
    enemy::{EnemyKind, EnemySpawner},
    player::xp::{LevelUpEvent, Xp},
    scheduling::PauseConditionExt,
};

/// Toggles whether or not we're rendering the Rapier debug lines.
fn toggle_rapier_debug(mut debug_ctx: ResMut<DebugRenderContext>) {
    debug_ctx.enabled = !debug_ctx.enabled;
}

/// Toggles whether or not the player is invulnerable.
#[derive(Parser, ConsoleCommand)]
#[command(name = "root")]
struct InvulnCommand;

/// Toggles whether or not the player is invulnerable.
fn player_invulnerable(
    mut commands: Commands,
    query: Query<(Entity, Option<&DebugInvulnerability>), With<Player>>,
    mut log: ConsoleCommand<InvulnCommand>,
) {
    if let Some(Ok(_)) = log.take() {
        for (entity, invuln) in &query {
            if invuln.is_some() {
                reply!(log, "invuln off");
                commands.entity(entity).remove::<DebugInvulnerability>();
            } else {
                reply!(log, "invuln on");
                commands.entity(entity).insert(DebugInvulnerability);
            }
        }
    }
}

/// Instantly gives the player a level up
#[derive(Parser, ConsoleCommand)]
#[command(name = "level_up")]
struct LevelUpCommand;

fn level_up(
    mut xp_query: Query<&mut Xp, With<Player>>,
    mut events: EventWriter<LevelUpEvent>,
    mut log: ConsoleCommand<LevelUpCommand>,
) {
    if let Some(Ok(_)) = log.take() {
        for mut xp in &mut xp_query {
            let remaining = xp.total_to_next_level() - xp.amount();
            xp.add(remaining);
        }
        events.send(LevelUpEvent);
        log.ok();
    }
}

/// Kills the player
#[derive(Parser, ConsoleCommand)]
#[command(name = "die")]
struct DieCommand;

/// Kills every enemy
#[derive(Parser, ConsoleCommand)]
#[command(name = "kill_9")]
struct KillEnemiesCommand;

/// Kills everything with the given component (i.e., `Player` or `Enemy`). This
/// deals damage equal to their current HP, so invulnerable entities will
/// survive.
fn kill_all<T: Component, Cmd>(
    query: Query<(Entity, &HP), With<T>>,
    mut events: EventWriter<IncomingDamageEvent>,
    mut log: ConsoleCommand<Cmd>,
) {
    if let Some(Ok(_)) = log.take() {
        let mut count = 0;
        for (entity, hp) in &query {
            events.send(IncomingDamageEvent {
                source: entity,
                target: entity,
                amount: hp.0,
                method: DamageMethod::Debug,
            });
            count += 1;
        }
        reply!(log, "killed {count} entities");
    }
}

/// Spawn some enemies near the player
#[derive(Parser, ConsoleCommand)]
#[command(name = "spawn")]
struct SpawnEnemyCommand {
    amount: usize,
    #[arg(value_enum)]
    kind: EnemyKind,
}

fn spawn_enemies(mut commands: Commands, mut log: ConsoleCommand<SpawnEnemyCommand>) {
    if let Some(Ok(SpawnEnemyCommand { amount, kind })) = log.take() {
        commands.spawn(EnemySpawner::one_shot(amount, kind));
        log.ok();
    }
}

/// Disables all spawned enemies and kills all existing enemies.
#[derive(Parser, ConsoleCommand)]
#[command(name = "sandbox")]
struct SandboxCommand;

#[allow(clippy::type_complexity)]
fn sandbox(
    mut commands: Commands,
    mut log: ConsoleCommand<SandboxCommand>,
    to_despawn: Query<Entity, Or<(With<EnemySpawner>, With<Enemy>)>>,
) {
    if let Some(Ok(_)) = log.take() {
        for to_despawn in &to_despawn {
            commands.entity(to_despawn).despawn_recursive();
        }
    }
}

/// Enables 'debug mode', which sets up keybindings for things like the bevy
/// inspector. This doesn't actually turn on any of the debugging features.
pub fn debug_mode_plugin(app: &mut App) {
    let inspector_keycode = KeyCode::Backslash;
    app.add_plugins(ConsolePlugin)
        .add_console_command::<InvulnCommand, _>(player_invulnerable)
        .add_console_command::<LevelUpCommand, _>(level_up)
        .add_console_command::<DieCommand, _>(kill_all::<Player, DieCommand>)
        .add_console_command::<KillEnemiesCommand, _>(kill_all::<Enemy, KillEnemiesCommand>)
        .add_console_command::<SpawnEnemyCommand, _>(spawn_enemies)
        .add_console_command::<SandboxCommand, _>(sandbox)
        .pause_if("Console", |res: Option<Res<ConsoleOpen>>| {
            res.map_or(false, |r| r.open)
        })
        .add_plugins((
            RapierDebugRenderPlugin::default().disabled(),
            WorldInspectorPlugin::new().run_if(input_toggle_active(false, inspector_keycode)),
        ))
        .add_systems(
            Update,
            toggle_rapier_debug.run_if(input_just_pressed(inspector_keycode)),
        );
}
