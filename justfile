# Run the pre-commit hooks as a linter
lint:
    .hooks/pre-commit

# Perform once-only repo setup
setup-repo:
    git config core.hooksPath .hooks
    direnv allow

# Build the host platform release
build-release:
    cargo build --release

# The reason we set CARGO_TARGET_DIR is so that it doesn't stomp all over the
# x86 assets.

# Build the wasm release
build-release-wasm:
   CARGO_TARGET_DIR=target-wasm trunk build --release --public-url /tengoku

# Deploy the wasm release; doesn't work unless you're me ;)
deploy-wasm: build-release-wasm
    CARGO_TARGET_DIR=target-wasm rsync --delete -av dist/ catgirl.ai:/var/www/catgirl.ai/tengoku

# Spin up a local wasm server
serve-wasm:
    CARGO_TARGET_DIR=target-wasm trunk serve -w src/ -w assets/


# Run all tests using `cargo nextest`
test:
    cargo nextest run
